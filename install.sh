#!/bin/bash

RECIPE_LATEX_HOME=$HOME/.texmf/tex/latex/recipe/
COMMON_LATEX_HOME=$HOME/.texmf/tex/latex/common/

mkdir -p $RECIPE_LATEX_HOME
mkdir -p $COMMON_LATEX_HOME

cp recipe-book.cls $RECIPE_LATEX_HOME
cp translation-English.dict $RECIPE_LATEX_HOME
cp multicol/*.sty $COMMON_LATEX_HOME
cp theme/*.png $RECIPE_LATEX_HOME

mktexlsr $HOME/.texmf

