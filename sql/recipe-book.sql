--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: recipe_book; Type: SCHEMA; Schema: -; Owner: recipe
--

CREATE SCHEMA recipe_book;


ALTER SCHEMA recipe_book OWNER TO recipe;

SET search_path = recipe_book, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: book; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book (
    id integer NOT NULL,
    label character varying(128) NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    deleted timestamp with time zone,
    updated timestamp with time zone
);


ALTER TABLE recipe_book.book OWNER TO recipe;

--
-- Name: TABLE book; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book IS 'Defines the common items to every book.';


--
-- Name: COLUMN book.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book.id IS 'Primary key.';


--
-- Name: COLUMN book.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book.label IS 'Book title.';


--
-- Name: COLUMN book.created; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book.created IS 'Date the book was created.';


--
-- Name: COLUMN book.deleted; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book.deleted IS 'Date the book was (or is to be?) deleted.';


--
-- Name: COLUMN book.updated; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book.updated IS 'Date the book was last updated.';


--
-- Name: book_account; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_account (
    id integer NOT NULL,
    book_id integer NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE recipe_book.book_account OWNER TO recipe;

--
-- Name: TABLE book_account; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_account IS 'Associates a book with an account.';


--
-- Name: COLUMN book_account.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_account.id IS 'Primary key.';


--
-- Name: COLUMN book_account.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_account.book_id IS 'The book associated with an account.';


--
-- Name: COLUMN book_account.account_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_account.account_id IS 'The account associated with a book.';


--
-- Name: book_account_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_account_id_seq OWNER TO recipe;

--
-- Name: book_account_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_account_id_seq OWNED BY book_account.id;


--
-- Name: book_aspect_name; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_aspect_name (
    id integer NOT NULL,
    code character varying(32) NOT NULL,
    label character varying(32) NOT NULL,
    description character varying(64) NOT NULL
);


ALTER TABLE recipe_book.book_aspect_name OWNER TO recipe;

--
-- Name: TABLE book_aspect_name; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_aspect_name IS 'Lists the names of book aspects. The aspects are a collection of snippets and many aspects are grouped together to form a theme. For example, COLOUR_SCHEME is an aspect of a theme. Think of this as the snippet category.';


--
-- Name: COLUMN book_aspect_name.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_aspect_name.id IS 'Primary key.';


--
-- Name: COLUMN book_aspect_name.code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_aspect_name.code IS 'Uniquely identifies the code in a language-neutral way.';


--
-- Name: COLUMN book_aspect_name.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_aspect_name.label IS 'Human-readable name for the aspect. Can be internationalized.';


--
-- Name: COLUMN book_aspect_name.description; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_aspect_name.description IS 'Brief description of how the aspect is meant to be used. Values in this column could be also displayed to the user.';


--
-- Name: book_aspect_name_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_aspect_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_aspect_name_id_seq OWNER TO recipe;

--
-- Name: book_aspect_name_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_aspect_name_id_seq OWNED BY book_aspect_name.id;


--
-- Name: book_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_id_seq OWNER TO recipe;

--
-- Name: book_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_id_seq OWNED BY book.id;


--
-- Name: book_isbn; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_isbn (
    id integer NOT NULL,
    book_id integer NOT NULL,
    isbn character varying(13) NOT NULL
);


ALTER TABLE recipe_book.book_isbn OWNER TO recipe;

--
-- Name: TABLE book_isbn; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_isbn IS 'ISBN for the given book.';


--
-- Name: COLUMN book_isbn.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_isbn.id IS 'Primary key.';


--
-- Name: COLUMN book_isbn.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_isbn.book_id IS 'Reference to the book that requires an ISBN code.';


--
-- Name: COLUMN book_isbn.isbn; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_isbn.isbn IS 'ISBN book number.';


--
-- Name: book_isbn_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_isbn_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_isbn_id_seq OWNER TO recipe;

--
-- Name: book_isbn_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_isbn_id_seq OWNED BY book_isbn.id;


--
-- Name: book_latex; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_latex (
    id integer NOT NULL,
    snippet_code character varying(32) NOT NULL,
    latex text NOT NULL
);


ALTER TABLE recipe_book.book_latex OWNER TO recipe;

--
-- Name: TABLE book_latex; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_latex IS 'Associates LaTeX code with a book preference.';


--
-- Name: COLUMN book_latex.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_latex.id IS 'Primary key.';


--
-- Name: COLUMN book_latex.latex; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_latex.latex IS 'Source code associated with a configurable preference.';


--
-- Name: book_latex_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_latex_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_latex_id_seq OWNER TO recipe;

--
-- Name: book_latex_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_latex_id_seq OWNED BY book_latex.id;


--
-- Name: book_photograph; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_photograph (
    id bigint NOT NULL,
    book_id bigint NOT NULL,
    photograph_id bigint NOT NULL
);


ALTER TABLE recipe_book.book_photograph OWNER TO recipe;

--
-- Name: TABLE book_photograph; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_photograph IS 'Associates photographs with books.';


--
-- Name: COLUMN book_photograph.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_photograph.id IS 'Primary key.';


--
-- Name: COLUMN book_photograph.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_photograph.book_id IS 'References a book.';


--
-- Name: COLUMN book_photograph.photograph_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_photograph.photograph_id IS 'References a photograph.';


--
-- Name: book_photograph_book_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_photograph_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_photograph_book_id_seq OWNER TO recipe;

--
-- Name: book_photograph_book_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_photograph_book_id_seq OWNED BY book_photograph.book_id;


--
-- Name: book_photograph_category_vw; Type: VIEW; Schema: recipe_book; Owner: recipe
--

CREATE VIEW book_photograph_category_vw AS
    SELECT bpc.id, bpc.label, bpc.description FROM recipe.photograph_category bpc WHERE ((bpc.classification)::text = 'RECIPE'::text) ORDER BY bpc.seq;


ALTER TABLE recipe_book.book_photograph_category_vw OWNER TO recipe;

--
-- Name: book_photograph_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_photograph_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_photograph_id_seq OWNER TO recipe;

--
-- Name: book_photograph_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_photograph_id_seq OWNED BY book_photograph.id;


--
-- Name: book_photograph_photograph_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_photograph_photograph_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_photograph_photograph_id_seq OWNER TO recipe;

--
-- Name: book_photograph_photograph_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_photograph_photograph_id_seq OWNED BY book_photograph.photograph_id;


--
-- Name: book_recipe; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_recipe (
    id integer NOT NULL,
    book_id integer NOT NULL,
    recipe_id integer NOT NULL,
    seq integer DEFAULT 0 NOT NULL
);


ALTER TABLE recipe_book.book_recipe OWNER TO recipe;

--
-- Name: TABLE book_recipe; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_recipe IS 'Associates a book with a recipe.';


--
-- Name: COLUMN book_recipe.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_recipe.id IS 'Primary key.';


--
-- Name: COLUMN book_recipe.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_recipe.book_id IS 'Reference to the book table.';


--
-- Name: COLUMN book_recipe.recipe_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_recipe.recipe_id IS 'Reference to the recipe table.';


--
-- Name: COLUMN book_recipe.seq; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_recipe.seq IS 'Determines the order of recipes in a book.';


--
-- Name: book_photograph_vw; Type: VIEW; Schema: recipe_book; Owner: recipe
--

CREATE VIEW book_photograph_vw AS
    (SELECT rp.id AS photograph_id, rbbr.book_id, rrp.recipe_id, rp.image_url, rp.photograph_category_id, rpc.label AS photograph_category FROM recipe.photograph rp, recipe.recipe_photograph rrp, book_recipe rbbr, recipe.photograph_category rpc WHERE ((((rp.id = rrp.photograph_id) AND (rrp.id = rbbr.recipe_id)) AND (rp.photograph_category_id = rpc.id)) AND ((rpc.classification)::text = 'RECIPE'::text)) UNION ALL SELECT rp.id AS photograph_id, rbbp.book_id, NULL::bigint AS recipe_id, rp.image_url, rp.photograph_category_id, rpc.label AS photograph_category FROM recipe.photograph rp, book_photograph rbbp, recipe.photograph_category rpc WHERE (((rp.id = rbbp.photograph_id) AND (rp.photograph_category_id = rpc.id)) AND ((rpc.classification)::text = 'BOOK'::text))) UNION ALL SELECT rp.id AS photograph_id, NULL::bigint AS book_id, rrp.recipe_id, rp.image_url, rp.photograph_category_id, rpc.label AS photograph_category FROM recipe.photograph rp, recipe.photograph_category rpc, recipe.recipe_photograph rrp WHERE (((rrp.photograph_id = rp.id) AND (rp.photograph_category_id = rpc.id)) AND (NOT (EXISTS (SELECT rbbr.recipe_id FROM book_recipe rbbr WHERE (rbbr.recipe_id = rrp.recipe_id)))));


ALTER TABLE recipe_book.book_photograph_vw OWNER TO recipe;

--
-- Name: book_recipe_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_recipe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_recipe_id_seq OWNER TO recipe;

--
-- Name: book_recipe_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_recipe_id_seq OWNED BY book_recipe.id;


--
-- Name: book_snippet; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_snippet (
    id integer NOT NULL,
    code character varying(32) NOT NULL,
    label character varying(32) NOT NULL,
    aspect_code character varying(32) DEFAULT 'COLOUR_SCHEME'::character varying NOT NULL
);


ALTER TABLE recipe_book.book_snippet OWNER TO recipe;

--
-- Name: TABLE book_snippet; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_snippet IS 'Lists the names of book snippets. The snippets are associated with specific aspects.';


--
-- Name: COLUMN book_snippet.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet.id IS 'Primary key.';


--
-- Name: COLUMN book_snippet.code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet.code IS 'Uniquely identifies the code in a language-neutral way.';


--
-- Name: COLUMN book_snippet.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet.label IS 'Human-readable name for the snippet. Can be internationalized.';


--
-- Name: COLUMN book_snippet.aspect_code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet.aspect_code IS 'Associates the book snippet with a book aspect.';


--
-- Name: book_snippet_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_snippet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_snippet_id_seq OWNER TO recipe;

--
-- Name: book_snippet_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_snippet_id_seq OWNED BY book_snippet.id;


--
-- Name: book_snippet_preference; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_snippet_preference (
    id integer NOT NULL,
    book_id integer NOT NULL,
    book_snippet_id integer NOT NULL
);


ALTER TABLE recipe_book.book_snippet_preference OWNER TO recipe;

--
-- Name: TABLE book_snippet_preference; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_snippet_preference IS 'Allows users to override aspects of a book theme by directly referencing a LaTeX snippet. For example, MODERN_SQUARE theme uses the LINUX_LIBERTINE font, but this could be overruled using the PAGELLA_ADVENTOR snippet (belonging to the FONT aspect).';


--
-- Name: COLUMN book_snippet_preference.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet_preference.id IS 'Primary key.';


--
-- Name: COLUMN book_snippet_preference.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet_preference.book_id IS 'Reference to the book that has some of its snippets overruled.';


--
-- Name: COLUMN book_snippet_preference.book_snippet_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet_preference.book_snippet_id IS 'Reference to the snippet that has overruled an aspect from the book''s theme preference.';


--
-- Name: book_snippet_preference_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_snippet_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_snippet_preference_id_seq OWNER TO recipe;

--
-- Name: book_snippet_preference_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_snippet_preference_id_seq OWNED BY book_snippet_preference.id;


--
-- Name: book_text; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_text (
    id bigint NOT NULL,
    book_id bigint NOT NULL,
    book_text_category_id integer NOT NULL,
    label character varying(2560)
);


ALTER TABLE recipe_book.book_text OWNER TO recipe;

--
-- Name: TABLE book_text; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_text IS 'Contains different book text types: tag overview (chapters), book overview (introduction), recipe overview, and so forth.';


--
-- Name: COLUMN book_text.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_text.id IS 'Primary key.';


--
-- Name: COLUMN book_text.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_text.book_id IS 'Foreign key to the book table.';


--
-- Name: COLUMN book_text.book_text_category_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_text.book_text_category_id IS 'Foreign key to the book_text_category table.';


--
-- Name: COLUMN book_text.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_text.label IS '250 words per page times 2 pages is 2500 characters, rounded up to 2560 to account for markup.';


--
-- Name: book_text_book_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_text_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_text_book_id_seq OWNER TO recipe;

--
-- Name: book_text_book_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_text_book_id_seq OWNED BY book_text.book_id;


--
-- Name: book_text_category; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_text_category (
    id integer NOT NULL,
    label character varying(32) NOT NULL,
    description character varying(128) NOT NULL
);


ALTER TABLE recipe_book.book_text_category OWNER TO recipe;

--
-- Name: TABLE book_text_category; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_text_category IS 'Lists book text categories.';


--
-- Name: COLUMN book_text_category.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_text_category.id IS 'Primary key.';


--
-- Name: COLUMN book_text_category.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_text_category.label IS 'Description of the category.';


--
-- Name: COLUMN book_text_category.description; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_text_category.description IS 'Description of how the label is intended to be used.';


--
-- Name: book_text_category_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_text_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_text_category_id_seq OWNER TO recipe;

--
-- Name: book_text_category_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_text_category_id_seq OWNED BY book_text_category.id;


--
-- Name: book_text_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_text_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_text_id_seq OWNER TO recipe;

--
-- Name: book_text_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_text_id_seq OWNED BY book_text.id;


--
-- Name: book_theme; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_theme (
    id integer NOT NULL,
    theme_code character varying(32) NOT NULL,
    aspect_code character varying(32) NOT NULL,
    snippet_code character varying(32) NOT NULL,
    seq integer DEFAULT 0 NOT NULL
);


ALTER TABLE recipe_book.book_theme OWNER TO recipe;

--
-- Name: TABLE book_theme; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_theme IS 'A book theme is comprised of a number of aspects. The aspects per theme must be unique. That is, a book theme cannot have two COLOUR_SCHEME aspects.';


--
-- Name: COLUMN book_theme.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme.id IS 'Primary key.';


--
-- Name: COLUMN book_theme.theme_code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme.theme_code IS 'Reference to the theme''s name.';


--
-- Name: COLUMN book_theme.aspect_code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme.aspect_code IS 'Reference to the aspect. Each aspect can only appear once in a theme.';


--
-- Name: COLUMN book_theme.snippet_code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme.snippet_code IS 'Indirect reference to the aspect code, via a snippet.';


--
-- Name: COLUMN book_theme.seq; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme.seq IS 'Queries for a book theme should fetch the results in the order denoted by this column.';


--
-- Name: book_theme_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_theme_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_theme_id_seq OWNER TO recipe;

--
-- Name: book_theme_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_theme_id_seq OWNED BY book_theme.id;


--
-- Name: book_theme_name; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_theme_name (
    id integer NOT NULL,
    code character varying(32) NOT NULL,
    label character varying NOT NULL
);


ALTER TABLE recipe_book.book_theme_name OWNER TO recipe;

--
-- Name: TABLE book_theme_name; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_theme_name IS 'Lists all the book theme names (e.g., maps MODERN_SQUARE to "Modern Square").';


--
-- Name: COLUMN book_theme_name.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_name.id IS 'Primary key.';


--
-- Name: COLUMN book_theme_name.code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_name.code IS 'Uniquely identifies the theme.';


--
-- Name: COLUMN book_theme_name.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_name.label IS 'Textual description of the book.';


--
-- Name: book_theme_name_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_theme_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_theme_name_id_seq OWNER TO recipe;

--
-- Name: book_theme_name_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_theme_name_id_seq OWNED BY book_theme_name.id;


--
-- Name: book_theme_preference; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_theme_preference (
    id integer NOT NULL,
    book_id integer NOT NULL,
    book_theme_name_id integer NOT NULL
);


ALTER TABLE recipe_book.book_theme_preference OWNER TO recipe;

--
-- Name: TABLE book_theme_preference; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_theme_preference IS 'Associates a book_them with a book.';


--
-- Name: COLUMN book_theme_preference.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_preference.id IS 'Primary key.';


--
-- Name: COLUMN book_theme_preference.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_preference.book_id IS 'Reference to an existing book.';


--
-- Name: COLUMN book_theme_preference.book_theme_name_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_preference.book_theme_name_id IS 'Reference to the theme that should be applied to the book.';


--
-- Name: book_theme_preference_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_theme_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_theme_preference_id_seq OWNER TO recipe;

--
-- Name: book_theme_preference_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_theme_preference_id_seq OWNED BY book_theme_preference.id;


--
-- Name: book_theme_vw; Type: VIEW; Schema: recipe_book; Owner: recipe
--

CREATE VIEW book_theme_vw AS
    SELECT rbbtn.id AS book_theme_name_id, rbbt.theme_code, rbbt.aspect_code, rbbt.snippet_code, rbbs.label, rbbl.latex FROM book_theme_name rbbtn, book_theme rbbt, book_snippet rbbs, book_latex rbbl WHERE ((((rbbtn.code)::text = (rbbt.theme_code)::text) AND ((rbbt.snippet_code)::text = (rbbs.code)::text)) AND ((rbbl.snippet_code)::text = (rbbs.code)::text)) ORDER BY rbbt.seq;


ALTER TABLE recipe_book.book_theme_vw OWNER TO recipe;

--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book ALTER COLUMN id SET DEFAULT nextval('book_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_account ALTER COLUMN id SET DEFAULT nextval('book_account_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_aspect_name ALTER COLUMN id SET DEFAULT nextval('book_aspect_name_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_isbn ALTER COLUMN id SET DEFAULT nextval('book_isbn_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_latex ALTER COLUMN id SET DEFAULT nextval('book_latex_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_photograph ALTER COLUMN id SET DEFAULT nextval('book_photograph_id_seq'::regclass);


--
-- Name: book_id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_photograph ALTER COLUMN book_id SET DEFAULT nextval('book_photograph_book_id_seq'::regclass);


--
-- Name: photograph_id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_photograph ALTER COLUMN photograph_id SET DEFAULT nextval('book_photograph_photograph_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_recipe ALTER COLUMN id SET DEFAULT nextval('book_recipe_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_snippet ALTER COLUMN id SET DEFAULT nextval('book_snippet_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_snippet_preference ALTER COLUMN id SET DEFAULT nextval('book_snippet_preference_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_text ALTER COLUMN id SET DEFAULT nextval('book_text_id_seq'::regclass);


--
-- Name: book_id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_text ALTER COLUMN book_id SET DEFAULT nextval('book_text_book_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_text_category ALTER COLUMN id SET DEFAULT nextval('book_text_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme ALTER COLUMN id SET DEFAULT nextval('book_theme_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme_name ALTER COLUMN id SET DEFAULT nextval('book_theme_name_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme_preference ALTER COLUMN id SET DEFAULT nextval('book_theme_preference_id_seq'::regclass);


--
-- Data for Name: book; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Data for Name: book_account; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Name: book_account_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_account_id_seq', 1, false);


--
-- Data for Name: book_aspect_name; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_aspect_name VALUES (3, 'FONT', 'Font Style', 'Sets the primary and secondary fonts.');
INSERT INTO book_aspect_name VALUES (7, 'PAGE_LAYOUT', 'Page Layout', 'Changes the arrangement of recipe parts on the page.');
INSERT INTO book_aspect_name VALUES (2, 'INSET_PHOTO', 'Inset Photographs', 'Decorates photographs on the same page as the recipe text.');
INSERT INTO book_aspect_name VALUES (10, 'UNCATEGORIZED', 'Uncategorized', 'Miscellaneous LaTeX snippets used by themes.');
INSERT INTO book_aspect_name VALUES (11, 'PDF', 'PDF Settings', 'Options for controlling various PDF settings.');
INSERT INTO book_aspect_name VALUES (12, 'COMMON', 'Common LaTeX', 'LaTeX source used by all themes; cannot be overridden by users.');
INSERT INTO book_aspect_name VALUES (1, 'COLOUR_SCHEME', 'Colour Scheme', 'Sets primary, secondary, and accent colours.');
INSERT INTO book_aspect_name VALUES (4, 'LIST_STYLE', 'List Style', 'Sets appearance for enumerated and bullet lists.');
INSERT INTO book_aspect_name VALUES (9, 'PAGE_HEADER', 'Page Header', 'Defines elements and style at the top of each page.');
INSERT INTO book_aspect_name VALUES (8, 'PAGE_FOOTER', 'Page Footer', 'Defines elements and style at the bottom of each page.');
INSERT INTO book_aspect_name VALUES (13, 'INDEX_STYLE_FLAT', 'Ingredient Index Style', 'Sets flat ingredient index formatting style.');
INSERT INTO book_aspect_name VALUES (14, 'INDEX_STYLE_CATEGORY', 'Categorized Index Style', 'Sets categorized ingredients index formatting style.');
INSERT INTO book_aspect_name VALUES (18, 'DIRECTIONS_LAYOUT', 'Directions Layout', 'Sets recipe instruction list formatting style.');
INSERT INTO book_aspect_name VALUES (16, 'EQUIPMENT_LAYOUT', 'Equipment Layout', 'Sets equipment list layout style.');
INSERT INTO book_aspect_name VALUES (17, 'PREPARATION_LAYOUT', 'Preparation Layout', 'Sets preparation list layout style.');
INSERT INTO book_aspect_name VALUES (15, 'INGREDIENT_LAYOUT', 'Ingredient Layout', 'Sets ingredient list layout style.');
INSERT INTO book_aspect_name VALUES (20, 'TOC_LAYOUT', 'Table of Contents layout', 'Specifies the style and layout of the table of contents.');


--
-- Name: book_aspect_name_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_aspect_name_id_seq', 20, true);


--
-- Name: book_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_id_seq', 1, false);


--
-- Data for Name: book_isbn; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Name: book_isbn_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_isbn_id_seq', 1, false);


--
-- Data for Name: book_latex; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_latex VALUES (7, 'SANS_SERIF', '\renewcommand{\recipefamily}{\sffamily}
\renewcommand{\headingfamily}{\sffamily}
\renewcommand{\familydefault}{\rmdefault}
');
INSERT INTO book_latex VALUES (9, 'SERIF_SANS', '\renewcommand{\recipefamily}{\rmfamily}
\renewcommand{\headingfamily}{\rmfamily}
\renewcommand{\familydefault}{\sfdefault}
');
INSERT INTO book_latex VALUES (10, 'SERIF', '\renewcommand{\recipefamily}{\rmfamily}
\renewcommand{\headingfamily}{\rmfamily}
\renewcommand{\familydefault}{\rmdefault}
');
INSERT INTO book_latex VALUES (14, 'ONE_COLUMN_SPREAD_REVERSE', '\renewenvironment{recipe}[1]{%
   \clearpage%
   \renewcommand{\recipetitle}{#1}
   \color{\maintextcolor}%
}{%
   \clearpage%
}

\renewcommand{\startinstructions}{}

\compactingredients
\compactequipment
%\compactinstructions

%\renewcommand{\startinstructions}{\columnbreak}
\renewcommand{\photo}[1]{%
%       \ThisCenterWallPaper{1}{#1}\cleardoublepage
        \fullpagephoto{#1}\cleardoublepage
        \section{\recipetitle}
');
INSERT INTO book_latex VALUES (1, 'CIURLIONIS', '% http://www.colourlovers.com/palette/2634294/APC393_%C4%8Ciurlionis
\definecolor[named]{skin}{HTML}{F2B166}
\definecolor[named]{nectar}{HTML}{EDE2C2}
\definecolor[named]{ifiwerestronger}{HTML}{D2B490}
\definecolor[named]{p}{HTML}{AB8369}
\definecolor[named]{suspect}{HTML}{755348}
\definecolor[named]{sky}{HTML}{1982D1}

\renewcommand{\primarycolor}{p}
\renewcommand{\secondarycolor}{ifiwerestronger}
\renewcommand{\tertiarycolor}{nectar}
\renewcommand{\darkhighlight}{suspect}
\renewcommand{\brighthighlight}{skin}
\renewcommand{\primarycolorcomplement}{black}
\renewcommand{\secondarycolorcomplement}{black}
\renewcommand{\tertiarycolorcomplement}{black}
\renewcommand{\externallinkcolor}{sky}
');
INSERT INTO book_latex VALUES (4, 'AUGIE', '% http://www.dafont.com/augie.font
\renewcommand{\rmdefault}{augie}
\renewcommand{\sfdefault}{augie}
\small
');
INSERT INTO book_latex VALUES (5, 'LINUX_LIBERTINE', '% Serif = Linux Libertine; Sans serif = Linux Biolinum
\renewcommand{\recipefamily}{\sffamily}
\renewcommand{\headingfamily}{\sffamily}
\renewcommand{\familydefault}{\rmdefault}

% http://www.linuxlibertine.org/
\usepackage[lf]{libertine}
');
INSERT INTO book_latex VALUES (6, 'PAGELLA_ADVENTOR', '% Serif = TeX Gyre Pagella (Palatino replacement)
% Sans serif = TeX Gyre Adventor (Avant Garde Gothic replacement)
% http://www.fontsquirrel.com/fonts/TeX-Gyre-Pagella
\usepackage{tgpagella}

% http://www.fontsquirrel.com/fonts/TeX-Gyre-Adventor
\usepackage[scale=.89]{tgadventor}
');
INSERT INTO book_latex VALUES (11, 'TERMES_HEROS', '% Sans serif = TeX Gyer Heros (Helvetica replacement)
% http://www.fontsquirrel.com/fonts/TeX-Gyre-Heros
\usepackage[scale=.89]{tgheros}

% Serif = Tex Gyre Termes (Times Roman replacement)
% http://www.fontsquirrel.com/fonts/TeX-Gyre-Termes
\usepackage{tgtermes}
');
INSERT INTO book_latex VALUES (2, 'CHOCOBERRY', '% http://www.colourlovers.com/palette/2634592/See_2
\definecolor[named]{foggy}{HTML}{F5F6CE}
\definecolor[named]{RosyAlpaca}{HTML}{DBB9A1}
\definecolor[named]{LoveMeFour}{HTML}{A45F62}
\definecolor[named]{one}{HTML}{71574B}
\definecolor[named]{PlataMate}{HTML}{9A957A}
\definecolor[named]{sky}{HTML}{1921D1}

\renewcommand{\primarycolor}{PlataMate}
\renewcommand{\secondarycolor}{RosyAlpaca!50}
\renewcommand{\tertiarycolor}{foggy}
\renewcommand{\primarycolorcomplement}{white}
\renewcommand{\secondarycolorcomplement}{black}
\renewcommand{\tertiarycolorcomplement}{black}
\renewcommand{\darkhighlight}{one}
\renewcommand{\brighthighlight}{LoveMeFour}
\renewcommand{\highlightcolor}{\brighthighlight}

');
INSERT INTO book_latex VALUES (3, 'SWEET_CANDY', '% http://www.colourlovers.com/palette/848743/(%E2%97%95_%E2%80%9D_%E2%97%95)
\definecolor[named]{PartyConfetti}{HTML}{490A3D}
\definecolor[named]{SugarHeartsYou}{HTML}{BD1550}
\definecolor[named]{SugarCocktail}{HTML}{E97F02}
\definecolor[named]{BurstsOfEuphoria}{HTML}{F8CA00}
\definecolor[named]{HappyBalloon}{HTML}{8A9B0F}
\definecolor[named]{sky}{HTML}{1921D1}

\renewcommand{\primarycolor}{SugarHeartsYou}
\renewcommand{\secondarycolor}{SugarCocktail}
\renewcommand{\tertiarycolor}{BurstsOfEuphoria}
\renewcommand{\primarycolorcomplement}{white}
\renewcommand{\secondarycolorcomplement}{white}
\renewcommand{\tertiarycolorcomplement}{black}
\renewcommand{\darkhighlight}{PartyConfetti}
\renewcommand{\brighthighlight}{HappyBalloon}
\renewcommand{\highlightcolor}{\brighthighlight}
\renewcommand{\externallinkcolor}{sky}');
INSERT INTO book_latex VALUES (8, 'SANS', '\renewcommand{\recipefamily}{\sffamily}
\renewcommand{\headingfamily}{\sffamily}
\renewcommand{\familydefault}{\sfdefault}
');
INSERT INTO book_latex VALUES (13, 'SQUARES', '\renewcommand{\headingmarker}{\tikz[baseline=-.26em]\node[diamond,draw=none,inner sep=.19em,fill=\headingcolor]{};}
\renewcommand{\itemmarker}{\textcolor{\secondarycolor}{\scriptsize$\blacksquare$}}
\renewcommand{\inlinelistmarker}{\makebox[1.5em][c]{\color{\secondarycolor}\tiny$\blacksquare$}}
\renewcommand{\enumeratemarker}[1]{\framedNum{#1}}
');
INSERT INTO book_latex VALUES (17, 'TWO_COLUMN_TWO_SIDE', '\renewcommand{\inlinecitationalignment}{%
  \iftoggle{print}{%
    \checkoddpage\strictpagecheck\ifoddpage\raggedright\else\raggedleft\fi
  }{%
    \raggedleft
  }
}

\renewenvironment{recipe}[2][]{%
   \clearpage%
   \parserecipecitation{#1}
   \color{\maintextcolor}%
   \raggedcolumns
   \iftoggle{print}{%
     \checkoddpage\strictpagecheck\ifoddpage\RLmulticolcolumns\else\LRmulticolcolumns\fi
   }{%
     \LRmulticolcolumns
   }
   \begin{multicols*}{2}[\section{#2}\checkinlinecitation]%
}{%
   \end{multicols*}%
}

\renewcommand{\startinstructions}{\columnbreak}

\normalingredients');
INSERT INTO book_latex VALUES (12, 'CIRCLES', '\renewcommand{\headingmarker}{\raisebox{1pt}{\small\CIRCLE}}
\renewcommand{\itemmarker}{\color{\secondarycolor}{\protect\CIRCLE}}
\renewcommand{\inlinelistmarker}{\raisebox{1pt}{\makebox[1.5em][c]{\color{\secondarycolor}\scriptsize\CIRCLE}}}
\renewcommand{\enumeratemarker}[1]{\circledNum{#1}}
');
INSERT INTO book_latex VALUES (38, 'LEFT_TOC', '\renewcommand\cftchapterfont{\headingfamily\bfseries\itshape\color{\primarycolor}\LARGE}
\renewcommand\cftchapterleader{\quad\itemmarker}
\renewcommand\cftchapterpagefont{\mdseries}
\renewcommand\cftchapterafterpnum{\hfill\cftparfillskip\vskip0.25em}
\setlength\cftbeforechapterskip{1.5em}

\renewcommand\cftsectionfont{}
\renewcommand\cftsectionleader{\quad\itemmarker}
\renewcommand\cftsectionpagefont{\mdseries}
\renewcommand\cftsectionafterpnum{\hfill\cftparfillskip}
\setlength\cftbeforesectionskip{0pt}
');
INSERT INTO book_latex VALUES (19, 'FRAMED_FOOTER', '\makeevenhead{headings}{}{}{}
\makeoddhead{headings}{}{}{}
\makeevenfoot{headings}{\framedPage}{}{}
\makeoddfoot{headings}{}{}{\framedPage}
\makeevenfoot{plain}{\framedPage}{}{}
\makeoddfoot{plain}{}{}{\framedPage}');
INSERT INTO book_latex VALUES (22, 'TOP_CATEGORY', '\begin{filecontents*}{deepidx.xdy}
(markup-locref :open "\hyperpage{" :close "}")

(markup-index :open  "~n\begin{theindex}~n"
              :close "~n\end{theindex}~n"
          :tree
              )
(markup-indexentry :open "~n  \indexspace~n  \item " :depth 0) 
(markup-indexentry :open "~n       \sssubitem " :depth 3) 
(markup-indexentry :open "~n            \sssssubitem "  :depth 4)
(markup-indexentry :open "~n            \sssssssubitem "  :depth 5)
(markup-keyword :open "\needspace{2\baselineskip}~n\protect\capitalizewords[q]{" :close "}{\large\headingfamily\bfseries\color{\primarycolor}\thestring}" :depth 0)
\end{filecontents*}

\long\def\lettergroup#1\item{\needspace{2\baselineskip}\item}
\let\lettergroupDefault\lettergroup');
INSERT INTO book_latex VALUES (21, 'LETTER_GROUP', '\begin{filecontents*}{flatidx.xdy}
(markup-locref :open "\hyperpage{" :close "}")

(markup-index :open  "~n\begin{theindex}~n"
              :close "~n\end{theindex}~n"
          :tree
              )

(markup-letter-group :open-head "\needspace{2\baselineskip}~n\textcolor{\primarycolor}{\large\headingfamily\bfseries " :close-head "}")
(markup-keyword :open "{\itshape "  :close "}" :depth 0)
\end{filecontents*}');
INSERT INTO book_latex VALUES (23, 'LESS_LIKE_MORE_LOVE', '% Based on http://www.colourlovers.com/palette/1199871/less_like_love_more
\definecolor[named]{redsarm}{HTML}{FF4500}
\definecolor[named]{lightyellow}{HTML}{FFFFE0}
\definecolor[named]{darkturquoise}{HTML}{00CED1}
\definecolor[named]{olivelight}{HTML}{ACC774}
\definecolor[named]{lillgray}{HTML}{4D454D}
\definecolor[named]{sky}{HTML}{1921D1}

\renewcommand{\primarycolor}{darkturquoise}
\renewcommand{\secondarycolor}{olivelight}
\renewcommand{\tertiarycolor}{lightyellow}
\renewcommand{\darkhighlight}{redsarm}
\renewcommand{\brighthighlight}{lightyellow}
\renewcommand{\primarycolorcomplement}{black}
\renewcommand{\secondarycolorcomplement}{black}
\renewcommand{\tertiarycolorcomplement}{black}
\renewcommand{\externallinkcolor}{sky}
');
INSERT INTO book_latex VALUES (15, 'UNDEFINED_MODERN', '% Lian Tze LIM
% http://liantze.penguinattack.org/
% 16 January 2013
%
% Layout inspiration: "100 Recipes from Japanese Cooking"
% Authors: Hata Koichiro and Kondo Kazuki
% Publisher: Kodansha Books 
% ISBN: 978-4770020796

\makeatletter
\providecommand{\vhrulefill}[1]{\leavevmode\leaders\hrule\@height#1\hfill \kern\z@}
\makeatother
\providecommand{\fullruled}[2][1pt]{#2\vspace{-.15em}\vhrulefill{#1}}
\providecommand{\halfruled}[2][.4pt]{\headingmarker\ #2\vspace{-.5em}\rule{.5\linewidth}{#1}\vspace{-.25em}}

\setaftersecskip{6pt}

%%%% Text-only cover
\renewcommand{\maketitle}{%
\begin{tikzpicture}[overlay,remember picture]
\fill[\primarycolor] ([xshift=-.5in]current page.north east) rectangle (current page.south east);
\draw[\primarycolor,line width=6pt] ([xshift=-.65in]current page.north east) -- ([xshift=-.65in]current page.south east);
\end{tikzpicture}
\vspace*{.5in}\par
\textcolor{\recipecolor}{\sffamily\fontsize{56pt}{58pt}\selectfont\raggedright{\thetitle}\\[.4em]\fullruled[2pt]{}}\par
\vfill
{\huge\itshape\color{\darkhighlight}A recipe compilation by \theauthor\par}
}

\setcolsepandrule{3em}{6pt}
\renewcommand{\columnseprulecolor}{\color{\tertiarycolor}}

%\chapterstyle{bianchi}
%\patchcommand{\printchaptertitle}{\color{\recipecolor}}{}
\makechapterstyle{thickbianchi}{%
  \chapterstyle{default}
  \renewcommand*{\chapnamefont}{\normalfont\Large\sffamily\itshape}
  \renewcommand*{\chapnumfont}{\normalfont\huge}
  \renewcommand*{\printchaptername}{%
    \chapnamefont\centering\@chapapp}
  \renewcommand*{\printchapternum}{\chapnumfont \textit{\thechapter}}
  \renewcommand*{\chaptitlefont}{\normalfont\Huge\sffamily}
  \renewcommand*{\printchaptertitle}[1]{%
    \color{\recipecolor}\vhrulefill{1pt}\\\centering \chaptitlefont\textbf{##1}\par}
  \renewcommand*{\afterchaptertitle}{\vspace{-.5\baselineskip}\vhrulefill{1pt}\\\vskip\afterchapskip}
  \renewcommand*{\printchapternonum}{%
    \vphantom{\chapnumfont \textit{9}}\afterchapternum}}
\chapterstyle{thickbianchi}
\setlength\beforechapskip{-\baselineskip}

\renewcommand{\recipesize}{\Huge}
\renewcommand{\recipestyle}{\bfseries\itshape\iftoggle{print}{\checkoddpage\strictpagecheck\ifoddpage\raggedleft\else\raggedright\fi}{\raggedright}\fullruled}
\renewcommand{\headingsize}{\large}
\renewcommand{\headingstyle}{\bfseries\halfruled}

\renewcommand{\formatcategorypage}[1]{%
\thispagestyle{empty}
\begin{tikzpicture}[overlay,remember picture]
\fill[\primarycolor,opacity=.5] ([xshift=-.6in]current page.north east) rectangle (current page.south east);
\draw[\primarycolor,opacity=.5,line width=6pt] ([xshift=-.8in]current page.north east) -- ([xshift=-.8in]current page.south east);
\end{tikzpicture}
\vfill
{\raggedleft%
\tikz[overlay]\node[font=\fontsize{160pt}{162pt}\itshape\selectfont,opacity=.2,text=\primarycolor,yshift=20pt]{\substring{#1}{1}{1}};
\fontsize{48pt}{50pt}\headingfamily\bfseries\itshape\selectfont\color{\recipecolor}#1\hspace*{.3in}\par}
\vskip6pt
\tikz[overlay,remember picture]\draw[line width=6pt,\primarycolor] (0,0 -| current page.east) -- (0,0 -| current page.west);
}

\renewcommand{\ovenstyle}{\itshape\par}

\renewcommand{\ingredientsstyle}{%
  nosep,%
  label=\itemmarker,%
  leftmargin=*
}

\renewcommand{\substitutionstyle}{%
  nosep,%
  label=\itemmarker,%
  leftmargin=*
}

\renewcommand{\preparationstyle}{%
  nosep,%
  label=\itemmarker,%
  leftmargin=*
}

\renewcommand{\equipmentstyle}{%
  mode=unboxed,%
  leftmargin=*,%
  itemjoin=\inlinelistmarker,
  nosep
}

\renewcommand{\instructionsstyle}{%
  leftmargin=*,%
  itemsep=1pt,%
  label={\enumeratemarker{\arabic*}}
}');
INSERT INTO book_latex VALUES (24, 'COMPACT_DIRECTIONS', '\compactinstructions
');
INSERT INTO book_latex VALUES (25, 'COMPACT_EQUIPMENT', '\compactequipment
');
INSERT INTO book_latex VALUES (26, 'COMPACT_INGREDIENTS', '\compactingredients
');
INSERT INTO book_latex VALUES (27, 'COMPACT_PREPARATION', '\compactpreparation
');
INSERT INTO book_latex VALUES (28, 'NORMAL_DIRECTIONS', '\normalinstructions
');
INSERT INTO book_latex VALUES (29, 'NORMAL_EQUIPMENT', '\normalequipment
');
INSERT INTO book_latex VALUES (30, 'NORMAL_INGREDIENTS', '\normalingredients
');
INSERT INTO book_latex VALUES (31, 'NORMAL_PREPARATION', '\normalpreparation
');
INSERT INTO book_latex VALUES (32, 'TABULAR_INGREDIENTS', '\tabularingredients
');
INSERT INTO book_latex VALUES (35, 'PLAIN_PHOTO', '\makeatletter
\renewcommand{\insetphoto}[2][]{%
   \parsephotocitation{#1}
   \includegraphics[width=\@photowidth]{#2}\par\vspace{\afterinsetphotoskip}
}
\makeatother
');
INSERT INTO book_latex VALUES (34, 'ROTATED_SHADOW', '%\pgfmathsetseed{\pdfuniformdeviate 10000000}
%\pgfmathsetseed{1}
\providecommand{\rotatepic}[2][\linewidth]{%
\noindent\raggedright%
\begin{tikzpicture}
\pgfmathsetlength{\tmplength}{.85 * #1}
\node[rotate={rand*4.0},inner sep=1em,draw=gray!60,fill=white, blur shadow={shadow blur steps=5,shadow xshift=1ex,shadow yshift=-1ex,shadow opacity=20,shadow blur radius=1ex}]{\includegraphics[width=\tmplength]{#2}};
\end{tikzpicture}
}

\makeatletter
\renewcommand{\insetphoto}[2][]{%
\parsephotocitation{#1}
\rotatepic[\@photowidth]{#2}\par\bigskip}
\makeatother');
INSERT INTO book_latex VALUES (36, 'TOP_BOTTOM', '\renewcommand{\inlinecitationalignment}{\centering}
\renewenvironment{recipe}[2][]{%
   \clearpage%
   \parserecipecitation{#1}
   \section{#2}
   \checkinlinecitation
   \color{\maintextcolor}%
   \raggedcolumns
   \begin{multicols}{2}\raggedright
}{%
   \clearpage%
}


\renewcommand{\startinstructions}{\end{multicols}}
%\renewcommand{\photo}[1]{%
% \insetphoto{#1}\columnbreak
%}

\compactequipment
\compactpreparation
\compactingredients
\compactinstructions
');
INSERT INTO book_latex VALUES (16, 'UNDEFINED_NOTEBOOK', '% Lian Tze LIM
% http://liantze.penguinattack.org/
% 16 January 2013

\usepackage{cookingsymbols}

\providecommand{\handUnderline}[1]{%
  \settowidth{\tmplength}{#1}%
  #1\vskip-.8em
  \tikz[decoration={random steps,segment length=2mm,amplitude=1pt}]\draw[decorate,line width=.15ex,line cap=round] (0,0) -- (\tmplength,0);\par
}

\providecommand{\scribbledHeading}[1]{%
  \handUnderline{\headingmarker\ #1}
}

\renewcommand{\recipesize}{\Huge}
\renewcommand{\recipestyle}[1]{\centering\handUnderline{#1}}
\renewcommand{\headingsize}{\large}
\renewcommand{\headingstyle}{\scribbledHeading}


%\setlength{\columnsep}{3em}
%\renewcommand{\columnseprule}{6pt}
\setcolsepandrule{3em}{6pt}
\renewcommand{\columnseprulecolor}{\color{\tertiarycolor}}

\renewcommand{\recipesize}{\Huge}
\renewcommand{\headingsize}{\Large}

\renewcommand{\headingmarker}{\textcolor{\headingcolor}{\S}}
\renewcommand{\itemmarker}{\textcolor{\secondarycolor}{*}}
\renewcommand{\enumeratemarker}[1]{\textcolor{\secondarycolor}{\# #1.}}
\renewcommand{\inlinelistmarker}{\makebox[1.5em][c]{\textcolor{\secondarycolor}{\textbullet}}}

\renewcommand{\ingredientsstyle}{%
  nosep,%
  label=\itemmarker,%
  leftmargin=*
}

\renewcommand{\preparationstyle}{%
  nosep,%
  label=\itemmarker,%
  leftmargin=*
}

\renewcommand\descriptionlabel[1]{%
  \hspace\labelsep
  \normalfont\textcolor{\darkhighlight}{#1:}}

\renewcommand{\equipmentstyle}{%
  mode=unboxed,%
  leftmargin=*,%
  itemjoin=\inlinelistmarker,
  afterlabel={\space},%
  nosep
}

\renewcommand{\instructionsstyle}{%
  leftmargin=*,%
  itemsep=1pt,%
  label={\enumeratemarker{\arabic*}}
}

\renewcommand{\substitutionstyle}{%
  nosep,%
  label=\itemmarker,%
  leftmargin=*
}


%\pgfmathsetseed{\pdfuniformdeviate 10000000}
%\pgfmathsetseed{1}
\providecommand{\rotatepic}[2][\linewidth]{%
\noindent\raggedright%
\begin{tikzpicture}
\pgfmathsetlength{\tmplength}{.85 * #1}
\node[rotate={rand*4.0},inner sep=1em,draw=gray!60,fill=white, blur shadow={shadow blur steps=5,shadow xshift=1ex,shadow yshift=-1ex,shadow opacity=20,shadow blur radius=1ex}]{\includegraphics[width=\tmplength]{#2}};
\end{tikzpicture}
}

\makeatletter
\renewcommand{\insetphoto}[2][]{%
\parsephotocitation{#1}
\rotatepic[\@photowidth]{#2}\par\bigskip}
\makeatother

%%%% Text-only cover
\renewcommand{\maketitle}{%
\pagecolor{\tertiarycolor}
\afterpage{\nopagecolor}
\begin{tikzpicture}[remember picture,overlay]
\fill[darkgray!60!black] ([xshift=-.9in]current page.north east) rectangle ([xshift=-1.4in]current page.south east);
\end{tikzpicture}
\vspace*{.3in}\par
\noindent%
\begin{minipage}{.9\textwidth}
{\color{\primarycolor}\sffamily\fontsize{40pt}{42pt}\selectfont\raggedright{\thetitle}\par}

\bigskip

{\LARGE\itshape\color{\darkhighlight}A recipe compilation by \theauthor\par}
\end{minipage}
}

\renewcommand{\formatcategorypage}[1]{%
\thispagestyle{empty}
\vspace*{\stretch{1}}
\begin{center}
{\fontsize{48pt}{50pt}\selectfont\color{\primarycolor}{\itemmarker\ #1\ \itemmarker}\par}
{\fontsize{150pt}{155pt}\selectfont\color{gray!70!white}\Fork\,\Knife\par}
\end{center}
\vspace*{\stretch{1}}
}
');
INSERT INTO book_latex VALUES (37, 'RASPBERRY_BLISS', '% http://www.colourlovers.com/palette/1015109/All_Eyes_On_You
\definecolor[named]{brokenheart}{HTML}{5C323E}
\definecolor[named]{pinkeyesonu}{HTML}{A82743}
\definecolor[named]{orangeeyesonu}{HTML}{E15E32}
\definecolor[named]{greeneyesonu}{HTML}{C0D23E}
\definecolor[named]{yelloweyesonu}{HTML}{E5F04C}

\renewcommand{\primarycolor}{pinkeyesonu}
\renewcommand{\secondarycolor}{greeneyesonu}
\renewcommand{\tertiarycolor}{orangeeyesonu}
\renewcommand{\darkhighlight}{brokenheart}
\renewcommand{\brighthighlight}{yelloweyesonu}
\renewcommand{\primarycolorcomplement}{black}
\renewcommand{\secondarycolorcomplement}{black}
\renewcommand{\tertiarycolorcomplement}{black}
');
INSERT INTO book_latex VALUES (40, 'ELEGANCE', '% http://www.colourlovers.com/palette/8007/Elegance
% http://www.colourlovers.com/palette/399315/An_Elegant_Aire
\definecolor[named]{ElegantLavendar}{HTML}{997788}
\definecolor[named]{ElegantLilac}{HTML}{BB99AA}
\definecolor[named]{SupremeSilver}{HTML}{ACB6BA}
\definecolor[named]{Elegance}{HTML}{E0E3E2}
\definecolor[named]{ChoiceSilver}{HTML}{E3E3E3}
\definecolor[named]{controlblue}{HTML}{180410}
\definecolor[named]{controldarkgreen}{HTML}{5C7D14}
\definecolor[named]{controlbrightgreen}{HTML}{BFD63A}
\definecolor[named]{controlpalegreen}{HTML}{D5E0CE}
\definecolor[named]{controlred}{HTML}{75011C}

\renewcommand{\primarycolor}{ElegantLavendar}
\renewcommand{\secondarycolor}{ElegantLilac}
\renewcommand{\tertiarycolor}{ChoiceSilver}
\renewcommand{\darkhighlight}{controlred}
\renewcommand{\brighthighlight}{SupremeSilver}
\renewcommand{\primarycolorcomplement}{ChoiceSilver}
\renewcommand{\secondarycolorcomplement}{ChoiceSilver}
\renewcommand{\tertiarycolorcomplement}{black}
');
INSERT INTO book_latex VALUES (18, 'TRUE_HYPERLINKS', '% PDF hyperlink style and configuration
\AtEndPreamble{\hypersetup{
  % Show bookmarks bar?
  bookmarks=true,
  % Non-Latin characters in bookmarks
  unicode=true,
  % Show toolbar?
  pdftoolbar=true,
  % Show menu?
  pdfmenubar=true,
  % Window fit to page when opened
  pdffitwindow=false,
  % Fit the width of the page to the window
  pdfstartview={FitH},
  % Title
  pdftitle={\thetitle},
  % author
  pdfauthor={\theauthor},
  % subject of the document
  pdfsubject={Recipes},
  % Creator of the document
  pdfcreator={RecipeZeal.com},
  % Producer of the document
  pdfproducer={pdflatex},
  % TODO: List of keywords
  pdfkeywords={recipe},
  % Links in new window
  pdfnewwindow=true,
  % false: boxed links; true: colored links
  colorlinks=true,
  % Color of internal links
  linkcolor=\primarycolor,
  % Color of links to bibliography
  citecolor=black,
  % Color of file links
  filecolor=black,
  % Color of external links
  urlcolor=\externallinkcolor,
  % No border around links
  pdfborder={0 0 0},
  % Make section titles and page numbers hyperlinked
  linktoc=all
}}
');
INSERT INTO book_latex VALUES (41, 'SCRIPT_SERIF', '\renewcommand{\recipefamily}{\scriptfamily}
\renewcommand{\headingfamily}{\scriptfamily}
\renewcommand{\familydefault}{\rmdefault}');
INSERT INTO book_latex VALUES (39, 'RIGHT_TOC', '\renewcommand\cftchapterfont{\headingfamily\bfseries\itshape\color{\primarycolor}\LARGE\hfill}
\renewcommand\cftchapterleader{\quad\itemmarker}
\renewcommand\cftchapterpagefont{\mdseries}
\renewcommand\cftchapterafterpnum{\cftparfillskip\vskip0.5em}
\setlength\cftbeforechapterskip{1.5em}

\renewcommand\cftsectionfont{\hfill}
\renewcommand\cftsectionleader{\quad\itemmarker}
\renewcommand\cftsectionpagefont{\mdseries}
\renewcommand\cftsectionafterpnum{\cftparfillskip}
\setlength\cftbeforesectionskip{0pt}
');
INSERT INTO book_latex VALUES (42, 'ORNATE_PLANT', '\usepackage{adforn,fourier-orns}
\renewcommand{\headingmarker}{\adftripleflourishleft}
\renewcommand{\itemmarker}{\color{\secondarycolor}{\adfhangingleafright}}
\renewcommand{\inlinelistmarker}{\ {\color{\brighthighlight}\adfast3}\ }
\renewcommand{\enumeratemarker}[1]{\textcolor{\brighthighlight}{\Large\scriptfamily#1.}\space}');
INSERT INTO book_latex VALUES (45, 'ELEGANT_FOOTER', '\usepackage{adforn,fourier-orns}
\setlength{\footskip}{3em}
\providecommand{\RuledPageNum}{\color{\darkhighlight}\adfdoubleflourishleft\ {\color{\maintextcolor}\scriptfamily\Large\thepage}\ \adfdoubleflourishright}
\makeevenfoot{headings}{}{\RuledPageNum}{}
\makeoddfoot{headings}{}{\RuledPageNum}{}
\makeevenfoot{plain}{}{\RuledPageNum}{}
\makeoddfoot{plain}{}{\RuledPageNum}{}');
INSERT INTO book_latex VALUES (46, 'ELEGANT_HEADLINE', '\usepackage{adforn,fourier-orns}
\setlength{\headsep}{.5em}
\setlength{\headheight}{2em}
\providecommand{\decoheadline}{\color{\tertiarycolor}\LARGE\hrulefill\ \textcolor{\darkhighlight}{\adfdoubleflourishleft\ \adfgee\ \adfdoubleflourishright}\ \hrulefill}
\makeevenhead{headings}{}{\decoheadline}{}
\makeoddhead{headings}{}{\decoheadline}{}
\makeevenhead{plain}{}{\decoheadline}{}
\makeoddhead{plain}{}{\decoheadline}{}
');
INSERT INTO book_latex VALUES (44, 'UNDEFINED_ELEGANCE', '\usepackage{ragged2e}

\providecommand{\semiornateHead}[1]{\raisebox{.5ex}{\makebox[\linewidth]{\hrulefill\ \raisebox{-.5ex}{\adfflowerleft~{\scriptfamily\LARGE\ #1}~\adfflowerright}\ \hrulefill}}}

\providecommand{\sedateornateHead}[1]{%
\centering{#1}\par
{\Large\adftripleflourishleft\par}	
}

\newcommand{\decoline}{%
\par{\centering\large
\textcolor{\headingcolor}{\adfflatleafoutlineleft\ \decotwo\ \adfflatleafoutlineright}\par}
}

\renewcommand{\dedication}[1]{%
\cleartoverso\thispagestyle{empty}
\vspace*{\stretch{1}}
\begin{center}
\itshape\Large
\textcolor{\primarycolor}{\headingmarker}\ %
\begin{minipage}[m]{.5\textwidth}\centering
#1\par%
\end{minipage}%
\ \textcolor{\primarycolor}{\headingmarker}\par%
\end{center}
\vspace*{\stretch{2}}
}

\renewcommand{\recipesize}{\fontsize{38pt}{40pt}\selectfont}
\renewcommand{\recipestyle}[1]{\sedateornateHead{#1}}
\renewcommand{\headingstyle}{\semiornateHead}


\setlength{\beforesubsecskip}{2em}

\setcolsepandrule{4em}{1pt}
\renewcommand{\columnseprulecolor}{\color{\tertiarycolor}}
\compactequipment
\compactingredients
\compactinstructions
\compactpreparation
\renewenvironment{ingredientlist}{%
   \begin{itemize*}[label={},afterlabel={},itemjoin={\ \inlinelistmarker\ },noitemsep]
}{%
   \end{itemize*}.
}

   \renewenvironment{instructionlist}{%
      \begin{enumerate*}[label={\enumeratemarker{\arabic*}},afterlabel={\space}]
   }{%
      \end{enumerate*}
   }

\AfterEndEnvironment{ingredients}{\needspace{3em}\centering\decoline}
\AfterEndEnvironment{equipment}{\needspace{3em}\centering\decoline}
\AfterEndEnvironment{substitution}{\needspace{3em}\centering\decoline}
\AfterEndEnvironment{preparation}{\needspace{3em}\centering\decoline}
\AfterEndEnvironment{instructions}{\justifying\decoline}


\providecommand{\ornatepic}[1]{%
\noindent\raggedright%
\begin{tikzpicture}
\node[inner sep=1.5em,draw=gray!60,fill=white, blur shadow={shadow blur steps=5,shadow xshift=1ex,shadow yshift=-1ex,shadow opacity=20,shadow blur radius=1ex}] (photo) {\includegraphics[width=.8\linewidth]{#1}};
\node[inner sep=0pt,font=\huge,anchor=north west,text=\darkhighlight,yshift=1.5em,xshift=-1.5em] (SE) at (photo.south east) {\adforn9};
\node[inner sep=0pt,font=\large,anchor=east,text=\darkhighlight] at (SE.west) {\adfflourishleftdouble};
\node[inner sep=0pt,font=\large,anchor=west,text=\darkhighlight,rotate=90] at (SE.north) {\adfflourishrightdouble};
\end{tikzpicture}
}

\makeatletter
\renewcommand{\insetphoto}[2][]{%
\parsephotocitation{#1}
\ornatepic{#2}\par\bigskip}
\makeatother

\renewcommand*{\beforechapskip}{-5em}
\renewcommand*{\chapnamefont}{\normalfont\Huge\scriptfamily\selectfont}
\renewcommand*{\chapnumfont}{\normalfont\huge\scriptfamily}
\renewcommand*{\printchaptername}{%
    \chapnamefont\centering\@chapapp}
\renewcommand*{\printchapternum}{\chapnumfont\thechapter}
\renewcommand*{\chaptitlefont}{\normalfont\fontsize{38pt}{40pt}\scriptfamily\selectfont}
\renewcommand*{\printchaptertitle}[1]{%
  	\chaptitlefont\color{\recipecolor}\sedateornateHead{#1}}
\renewcommand*{\printchapternonum}{%
  \vphantom{\chapnumfont \textit{9}}\afterchapternum}');


--
-- Name: book_latex_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_latex_id_seq', 46, true);


--
-- Data for Name: book_photograph; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Name: book_photograph_book_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_photograph_book_id_seq', 1, false);


--
-- Name: book_photograph_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_photograph_id_seq', 1, false);


--
-- Name: book_photograph_photograph_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_photograph_photograph_id_seq', 1, false);


--
-- Data for Name: book_recipe; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Name: book_recipe_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_recipe_id_seq', 1, false);


--
-- Data for Name: book_snippet; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_snippet VALUES (1, 'CIURLIONIS', 'Čiurlionis', 'COLOUR_SCHEME');
INSERT INTO book_snippet VALUES (2, 'CHOCOBERRY', 'Chocoberry', 'COLOUR_SCHEME');
INSERT INTO book_snippet VALUES (3, 'SWEET_CANDY', 'Sweet Candy', 'COLOUR_SCHEME');
INSERT INTO book_snippet VALUES (5, 'LINUX_LIBERTINE', 'Penguin Liberty', 'FONT');
INSERT INTO book_snippet VALUES (6, 'PAGELLA_ADVENTOR', 'Leafy Adventure', 'FONT');
INSERT INTO book_snippet VALUES (4, 'AUGIE', 'Third Grade', 'FONT');
INSERT INTO book_snippet VALUES (7, 'SANS_SERIF', 'Sans over Serif', 'FONT');
INSERT INTO book_snippet VALUES (8, 'SANS', 'Sans Alone', 'FONT');
INSERT INTO book_snippet VALUES (9, 'SERIF_SANS', 'Serif over Sans', 'FONT');
INSERT INTO book_snippet VALUES (10, 'SERIF', 'Serif Alone', 'FONT');
INSERT INTO book_snippet VALUES (11, 'TERMES_HEROS', 'Old Time Héros', 'FONT');
INSERT INTO book_snippet VALUES (12, 'CIRCLES', 'Circles', 'LIST_STYLE');
INSERT INTO book_snippet VALUES (13, 'SQUARES', 'Squares', 'LIST_STYLE');
INSERT INTO book_snippet VALUES (14, 'ONE_COLUMN_SPREAD_REVERSE', 'Full Page Pictures', 'PAGE_LAYOUT');
INSERT INTO book_snippet VALUES (15, 'FRAMED_FOOTER', 'Framed Page Number', 'PAGE_FOOTER');
INSERT INTO book_snippet VALUES (16, 'UNDEFINED_MODERN', 'Undefined Modern', 'UNCATEGORIZED');
INSERT INTO book_snippet VALUES (17, 'UNDEFINED_NOTEBOOK', 'Undefined Notebook', 'UNCATEGORIZED');
INSERT INTO book_snippet VALUES (18, 'TWO_COLUMN_TWO_SIDE', 'Half and Half', 'PAGE_LAYOUT');
INSERT INTO book_snippet VALUES (19, 'TRUE_HYPERLINKS', 'Regular Hyperlink Style', 'PDF');
INSERT INTO book_snippet VALUES (20, 'LATEX_COMMON', 'Common LaTeX Code', 'COMMON');
INSERT INTO book_snippet VALUES (21, 'LETTER_GROUP', 'Letter Group', 'INDEX_STYLE_FLAT');
INSERT INTO book_snippet VALUES (22, 'TOP_CATEGORY', 'Top Category', 'INDEX_STYLE_CATEGORY');
INSERT INTO book_snippet VALUES (23, 'LESS_LIKE_MORE_LOVE', 'Neapolitan Forest', 'COLOUR_SCHEME');
INSERT INTO book_snippet VALUES (24, 'NORMAL_DIRECTIONS', 'Normal Directions', 'DIRECTIONS_LAYOUT');
INSERT INTO book_snippet VALUES (25, 'COMPACT_DIRECTIONS', 'Compact Directions', 'DIRECTIONS_LAYOUT');
INSERT INTO book_snippet VALUES (26, 'NORMAL_EQUIPMENT', 'Normal Equipment', 'EQUIPMENT_LAYOUT');
INSERT INTO book_snippet VALUES (27, 'COMPACT_EQUIPMENT', 'Compact Equipment', 'EQUIPMENT_LAYOUT');
INSERT INTO book_snippet VALUES (28, 'NORMAL_INGREDIENTS', 'Normal Ingredients', 'INGREDIENT_LAYOUT');
INSERT INTO book_snippet VALUES (29, 'COMPACT_INGREDIENTS', 'Compact Ingredients', 'INGREDIENT_LAYOUT');
INSERT INTO book_snippet VALUES (30, 'TABULAR_INGREDIENTS', 'Tabular Ingredients', 'INGREDIENT_LAYOUT');
INSERT INTO book_snippet VALUES (31, 'NORMAL_PREPARATION', 'Normal Preparation', 'PREPARATION_LAYOUT');
INSERT INTO book_snippet VALUES (32, 'COMPACT_PREPARATION', 'Compact Preparation', 'PREPARATION_LAYOUT');
INSERT INTO book_snippet VALUES (34, 'ROTATED_SHADOW', 'Rotated Shadow', 'INSET_PHOTO');
INSERT INTO book_snippet VALUES (33, 'PLAIN_PHOTO', 'Plain Photo', 'INSET_PHOTO');
INSERT INTO book_snippet VALUES (35, 'TOP_BOTTOM', 'Top and Bottom', 'PAGE_LAYOUT');
INSERT INTO book_snippet VALUES (36, 'RASPBERRY_BLISS', 'Raspberry Bliss', 'COLOUR_SCHEME');
INSERT INTO book_snippet VALUES (37, 'LEFT_TOC', 'Left-aligned ToC', 'TOC_LAYOUT');
INSERT INTO book_snippet VALUES (38, 'RIGHT_TOC', 'Right-aligned ToC', 'TOC_LAYOUT');
INSERT INTO book_snippet VALUES (39, 'ELEGANCE', 'Elegance', 'COLOUR_SCHEME');
INSERT INTO book_snippet VALUES (40, 'SCRIPT_SERIF', 'Script over Serif', 'FONT');
INSERT INTO book_snippet VALUES (41, 'ORNATE_PLANT', 'Ornate Plants', 'LIST_STYLE');
INSERT INTO book_snippet VALUES (42, 'UNDEFINED_ELEGANCE', 'Undefined Elegance', 'UNCATEGORIZED');
INSERT INTO book_snippet VALUES (43, 'ELEGANT_HEADLINE', 'Elegant Headlin', 'PAGE_HEADER');
INSERT INTO book_snippet VALUES (44, 'ELEGANT_FOOTER', 'Elegant Footer', 'PAGE_FOOTER');


--
-- Name: book_snippet_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_snippet_id_seq', 44, true);


--
-- Data for Name: book_snippet_preference; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Name: book_snippet_preference_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_snippet_preference_id_seq', 1, false);


--
-- Data for Name: book_text; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Name: book_text_book_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_text_book_id_seq', 1, false);


--
-- Data for Name: book_text_category; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_text_category VALUES (3, 'recipe', 'Recipe overview, usually to introduce a recipe.');
INSERT INTO book_text_category VALUES (1, 'tag', 'Chapter overview, usually associated with a recipe tag (section).');
INSERT INTO book_text_category VALUES (2, 'book', 'Book overview, usually a book''s opening content.');


--
-- Name: book_text_category_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_text_category_id_seq', 5, true);


--
-- Name: book_text_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_text_id_seq', 1, false);


--
-- Data for Name: book_theme; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_theme VALUES (1, 'MODERN_SQUARE', 'FONT', 'LINUX_LIBERTINE', 2);
INSERT INTO book_theme VALUES (6, 'MODERN_SQUARE', 'UNCATEGORIZED', 'UNDEFINED_MODERN', 7);
INSERT INTO book_theme VALUES (5, 'MODERN_SQUARE', 'PAGE_FOOTER', 'FRAMED_FOOTER', 6);
INSERT INTO book_theme VALUES (4, 'MODERN_SQUARE', 'PAGE_LAYOUT', 'TWO_COLUMN_TWO_SIDE', 5);
INSERT INTO book_theme VALUES (33, 'ELEGANCE', 'FONT', 'SCRIPT_SERIF', 1);
INSERT INTO book_theme VALUES (9, 'MODERN_SQUARE', 'INDEX_STYLE_FLAT', 'LETTER_GROUP', 9);
INSERT INTO book_theme VALUES (10, 'MODERN_SQUARE', 'INDEX_STYLE_CATEGORY', 'TOP_CATEGORY', 10);
INSERT INTO book_theme VALUES (2, 'MODERN_SQUARE', 'COLOUR_SCHEME', 'CHOCOBERRY', 3);
INSERT INTO book_theme VALUES (11, 'MODERN_SQUARE', 'INSET_PHOTO', 'PLAIN_PHOTO', 11);
INSERT INTO book_theme VALUES (12, 'NOTEBOOK', 'FONT', 'AUGIE', 1);
INSERT INTO book_theme VALUES (13, 'NOTEBOOK', 'PAGE_LAYOUT', 'TOP_BOTTOM', 2);
INSERT INTO book_theme VALUES (14, 'NOTEBOOK', 'COLOUR_SCHEME', 'SWEET_CANDY', 3);
INSERT INTO book_theme VALUES (15, 'NOTEBOOK', 'PAGE_FOOTER', 'FRAMED_FOOTER', 4);
INSERT INTO book_theme VALUES (16, 'NOTEBOOK', 'INDEX_STYLE_FLAT', 'LETTER_GROUP', 5);
INSERT INTO book_theme VALUES (17, 'NOTEBOOK', 'INDEX_STYLE_CATEGORY', 'TOP_CATEGORY', 6);
INSERT INTO book_theme VALUES (18, 'NOTEBOOK', 'UNCATEGORIZED', 'UNDEFINED_NOTEBOOK', 7);
INSERT INTO book_theme VALUES (21, 'MODERN_CIRCLE', 'FONT', 'TERMES_HEROS', 2);
INSERT INTO book_theme VALUES (20, 'MODERN_CIRCLE', 'COLOUR_SCHEME', 'RASPBERRY_BLISS', 3);
INSERT INTO book_theme VALUES (22, 'MODERN_CIRCLE', 'PAGE_LAYOUT', 'TWO_COLUMN_TWO_SIDE', 5);
INSERT INTO book_theme VALUES (24, 'MODERN_CIRCLE', 'UNCATEGORIZED', 'UNDEFINED_MODERN', 7);
INSERT INTO book_theme VALUES (34, 'ELEGANCE', 'COLOUR_SCHEME', 'ELEGANCE', 2);
INSERT INTO book_theme VALUES (3, 'MODERN_SQUARE', 'LIST_STYLE', 'SQUARES', 4);
INSERT INTO book_theme VALUES (35, 'ELEGANCE', 'PAGE_LAYOUT', 'TWO_COLUMN_TWO_SIDE', 3);
INSERT INTO book_theme VALUES (25, 'MODERN_CIRCLE', 'LIST_STYLE', 'CIRCLES', 4);
INSERT INTO book_theme VALUES (26, 'MODERN_CIRCLE', 'INSET_PHOTO', 'PLAIN_PHOTO', 11);
INSERT INTO book_theme VALUES (28, 'MODERN_CIRCLE', 'INDEX_STYLE_CATEGORY', 'TOP_CATEGORY', 10);
INSERT INTO book_theme VALUES (29, 'MODERN_CIRCLE', 'INDEX_STYLE_FLAT', 'LETTER_GROUP', 9);
INSERT INTO book_theme VALUES (23, 'MODERN_CIRCLE', 'PDF', 'TRUE_HYPERLINKS', 12);
INSERT INTO book_theme VALUES (19, 'NOTEBOOK', 'PDF', 'TRUE_HYPERLINKS', 12);
INSERT INTO book_theme VALUES (7, 'MODERN_SQUARE', 'PDF', 'TRUE_HYPERLINKS', 12);
INSERT INTO book_theme VALUES (31, 'MODERN_CIRCLE', 'TOC_LAYOUT', 'LEFT_TOC', 8);
INSERT INTO book_theme VALUES (30, 'MODERN_SQUARE', 'TOC_LAYOUT', 'LEFT_TOC', 8);
INSERT INTO book_theme VALUES (32, 'NOTEBOOK', 'TOC_LAYOUT', 'RIGHT_TOC', 8);
INSERT INTO book_theme VALUES (36, 'ELEGANCE', 'LIST_STYLE', 'ORNATE_PLANT', 4);
INSERT INTO book_theme VALUES (39, 'ELEGANCE', 'INDEX_STYLE_CATEGORY', 'TOP_CATEGORY', 7);
INSERT INTO book_theme VALUES (40, 'ELEGANCE', 'INDEX_STYLE_FLAT', 'LETTER_GROUP', 8);
INSERT INTO book_theme VALUES (41, 'ELEGANCE', 'TOC_LAYOUT', 'RIGHT_TOC', 10);
INSERT INTO book_theme VALUES (42, 'ELEGANCE', 'PDF', 'TRUE_HYPERLINKS', 11);
INSERT INTO book_theme VALUES (44, 'ELEGANCE', 'UNCATEGORIZED', 'UNDEFINED_ELEGANCE', 6);
INSERT INTO book_theme VALUES (45, 'ELEGANCE', 'PAGE_HEADER', 'ELEGANT_HEADLINE', 6);
INSERT INTO book_theme VALUES (46, 'ELEGANCE', 'PAGE_FOOTER', 'ELEGANT_FOOTER', 5);
INSERT INTO book_theme VALUES (47, 'ELEGANCE', 'INGREDIENT_LAYOUT', 'NORMAL_INGREDIENTS', 12);
INSERT INTO book_theme VALUES (48, 'ELEGANCE', 'DIRECTIONS_LAYOUT', 'NORMAL_DIRECTIONS', 13);
INSERT INTO book_theme VALUES (49, 'ELEGANCE', 'PREPARATION_LAYOUT', 'NORMAL_PREPARATION', 14);


--
-- Name: book_theme_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_theme_id_seq', 49, true);


--
-- Data for Name: book_theme_name; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_theme_name VALUES (1, 'MODERN_SQUARE', 'Modern Square');
INSERT INTO book_theme_name VALUES (2, 'MODERN_CIRCLE', 'Modern Circle');
INSERT INTO book_theme_name VALUES (3, 'NOTEBOOK', 'Notebook');
INSERT INTO book_theme_name VALUES (4, 'ELEGANCE', 'Elegance');


--
-- Name: book_theme_name_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_theme_name_id_seq', 4, true);


--
-- Data for Name: book_theme_preference; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Name: book_theme_preference_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_theme_preference_id_seq', 1, false);


--
-- Name: pk_book; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book
    ADD CONSTRAINT pk_book PRIMARY KEY (id);


--
-- Name: pk_book_account; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_account
    ADD CONSTRAINT pk_book_account PRIMARY KEY (id);


--
-- Name: pk_book_aspect_name; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_aspect_name
    ADD CONSTRAINT pk_book_aspect_name PRIMARY KEY (id);


--
-- Name: pk_book_latex; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_latex
    ADD CONSTRAINT pk_book_latex PRIMARY KEY (id);


--
-- Name: pk_book_photograph; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_photograph
    ADD CONSTRAINT pk_book_photograph PRIMARY KEY (id);


--
-- Name: pk_book_recipe; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_recipe
    ADD CONSTRAINT pk_book_recipe PRIMARY KEY (id);


--
-- Name: pk_book_snippet; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_snippet
    ADD CONSTRAINT pk_book_snippet PRIMARY KEY (id);


--
-- Name: pk_book_snippet_preference; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_snippet_preference
    ADD CONSTRAINT pk_book_snippet_preference PRIMARY KEY (id);


--
-- Name: pk_book_text; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_text
    ADD CONSTRAINT pk_book_text PRIMARY KEY (id);


--
-- Name: pk_book_text_category; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_text_category
    ADD CONSTRAINT pk_book_text_category PRIMARY KEY (id);


--
-- Name: pk_book_theme; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme
    ADD CONSTRAINT pk_book_theme PRIMARY KEY (id);


--
-- Name: pk_book_theme_name; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme_name
    ADD CONSTRAINT pk_book_theme_name PRIMARY KEY (id);


--
-- Name: pk_book_theme_preference; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme_preference
    ADD CONSTRAINT pk_book_theme_preference PRIMARY KEY (id);


--
-- Name: uk_book_aspect_code; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_aspect_name
    ADD CONSTRAINT uk_book_aspect_code UNIQUE (code);


--
-- Name: uk_book_aspect_label; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_aspect_name
    ADD CONSTRAINT uk_book_aspect_label UNIQUE (label);


--
-- Name: uk_book_snippet_code; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_snippet
    ADD CONSTRAINT uk_book_snippet_code UNIQUE (code);


--
-- Name: uk_book_snippet_label; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_snippet
    ADD CONSTRAINT uk_book_snippet_label UNIQUE (label);


--
-- Name: uk_book_theme_name_code; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme_name
    ADD CONSTRAINT uk_book_theme_name_code UNIQUE (code);


--
-- Name: uk_book_theme_preference_book; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme_preference
    ADD CONSTRAINT uk_book_theme_preference_book UNIQUE (book_id, book_theme_name_id);


--
-- Name: uk_book_theme_theme_code_aspect_code; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme
    ADD CONSTRAINT uk_book_theme_theme_code_aspect_code UNIQUE (theme_code, aspect_code);


--
-- Name: idx_book_account; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_account ON book_account USING btree (book_id);


--
-- Name: idx_book_account_0; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_account_0 ON book_account USING btree (account_id);


--
-- Name: idx_book_latex; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_latex ON book_latex USING btree (snippet_code);


--
-- Name: idx_book_recipe; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_recipe ON book_recipe USING btree (recipe_id);


--
-- Name: idx_book_recipe_0; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_recipe_0 ON book_recipe USING btree (book_id);


--
-- Name: idx_book_theme; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_theme ON book_theme USING btree (theme_code);


--
-- Name: idx_book_theme_0; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_theme_0 ON book_theme USING btree (snippet_code);


--
-- Name: fk_book_account_account_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_account
    ADD CONSTRAINT fk_book_account_account_id FOREIGN KEY (account_id) REFERENCES recipe.account(id) ON DELETE CASCADE;


--
-- Name: fk_book_account_book_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_account
    ADD CONSTRAINT fk_book_account_book_id FOREIGN KEY (book_id) REFERENCES book(id) ON DELETE CASCADE;


--
-- Name: fk_book_latex_book_snippet; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_latex
    ADD CONSTRAINT fk_book_latex_book_snippet FOREIGN KEY (snippet_code) REFERENCES book_snippet(code) ON DELETE CASCADE;


--
-- Name: fk_book_photograph_book_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_photograph
    ADD CONSTRAINT fk_book_photograph_book_id FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: fk_book_photograph_photograph_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_photograph
    ADD CONSTRAINT fk_book_photograph_photograph_id FOREIGN KEY (photograph_id) REFERENCES recipe.photograph(id);


--
-- Name: fk_book_recipe_book_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_recipe
    ADD CONSTRAINT fk_book_recipe_book_id FOREIGN KEY (book_id) REFERENCES book(id) ON DELETE CASCADE;


--
-- Name: fk_book_recipe_recipe_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_recipe
    ADD CONSTRAINT fk_book_recipe_recipe_id FOREIGN KEY (recipe_id) REFERENCES recipe.recipe(id) ON DELETE CASCADE;


--
-- Name: fk_book_snippet_code_aspect_code; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_snippet
    ADD CONSTRAINT fk_book_snippet_code_aspect_code FOREIGN KEY (aspect_code) REFERENCES book_aspect_name(code);


--
-- Name: fk_book_snippet_preference_book_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_snippet_preference
    ADD CONSTRAINT fk_book_snippet_preference_book_id FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: fk_book_snippet_preference_book_snippet_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_snippet_preference
    ADD CONSTRAINT fk_book_snippet_preference_book_snippet_id FOREIGN KEY (book_snippet_id) REFERENCES book_snippet(id);


--
-- Name: fk_book_text_book_text_category_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_text
    ADD CONSTRAINT fk_book_text_book_text_category_id FOREIGN KEY (book_text_category_id) REFERENCES book_text_category(id);


--
-- Name: fk_book_theme_book_aspect_name; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme
    ADD CONSTRAINT fk_book_theme_book_aspect_name FOREIGN KEY (aspect_code) REFERENCES book_aspect_name(code) ON DELETE CASCADE;


--
-- Name: fk_book_theme_book_snippet; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme
    ADD CONSTRAINT fk_book_theme_book_snippet FOREIGN KEY (snippet_code) REFERENCES book_snippet(code) ON DELETE CASCADE;


--
-- Name: fk_book_theme_book_theme_name; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme
    ADD CONSTRAINT fk_book_theme_book_theme_name FOREIGN KEY (theme_code) REFERENCES book_theme_name(code) ON DELETE CASCADE;


--
-- Name: fk_book_theme_preference_book; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme_preference
    ADD CONSTRAINT fk_book_theme_preference_book FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: fk_book_theme_preference_book_theme_name_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme_preference
    ADD CONSTRAINT fk_book_theme_preference_book_theme_name_id FOREIGN KEY (book_theme_name_id) REFERENCES book_theme_name(id);


--
-- PostgreSQL database dump complete
--

