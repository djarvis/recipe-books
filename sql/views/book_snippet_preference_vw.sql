﻿-- DROP VIEW recipe_book.book_snippet_preference_vw;

CREATE OR REPLACE VIEW recipe_book.book_snippet_preference_vw AS
SELECT
  rbbs.id,
  rbbs.label,
  rbbs.code,
  rbbs.aspect_code
FROM
  recipe_book.book_snippet rbbs
WHERE
  rbbs.aspect_code IN (
    'COLOUR_SCHEME',
    'FONT'
  )
ORDER BY
  rbbs.aspect_code,
  rbbs.label;
