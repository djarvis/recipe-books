--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: recipe_book; Type: SCHEMA; Schema: -; Owner: recipe
--

CREATE SCHEMA recipe_book;


ALTER SCHEMA recipe_book OWNER TO recipe;

SET search_path = recipe_book, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: book; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book (
    id integer NOT NULL,
    label character varying(128) NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    deleted timestamp with time zone,
    updated timestamp with time zone
);


ALTER TABLE recipe_book.book OWNER TO recipe;

--
-- Name: TABLE book; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book IS 'Defines the common items to every book.';


--
-- Name: COLUMN book.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book.id IS 'Primary key.';


--
-- Name: COLUMN book.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book.label IS 'Book title.';


--
-- Name: COLUMN book.created; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book.created IS 'Date the book was created.';


--
-- Name: COLUMN book.deleted; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book.deleted IS 'Date the book was (or is to be?) deleted.';


--
-- Name: COLUMN book.updated; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book.updated IS 'Date the book was last updated.';


--
-- Name: book_account; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_account (
    id integer NOT NULL,
    book_id integer NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE recipe_book.book_account OWNER TO recipe;

--
-- Name: TABLE book_account; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_account IS 'Associates a book with an account.';


--
-- Name: COLUMN book_account.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_account.id IS 'Primary key.';


--
-- Name: COLUMN book_account.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_account.book_id IS 'The book associated with an account.';


--
-- Name: COLUMN book_account.account_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_account.account_id IS 'The account associated with a book.';


--
-- Name: book_account_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_account_id_seq OWNER TO recipe;

--
-- Name: book_account_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_account_id_seq OWNED BY book_account.id;


--
-- Name: book_account_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_account_id_seq', 19, true);


--
-- Name: book_aspect_name; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_aspect_name (
    id integer NOT NULL,
    code character varying(32) NOT NULL,
    label character varying(32) NOT NULL,
    description character varying(64) NOT NULL
);


ALTER TABLE recipe_book.book_aspect_name OWNER TO recipe;

--
-- Name: TABLE book_aspect_name; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_aspect_name IS 'Lists the names of book aspects. The aspects are a collection of snippets and many aspects are grouped together to form a theme. For example, COLOUR_SCHEME is an aspect of a theme. Think of this as the snippet category.';


--
-- Name: COLUMN book_aspect_name.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_aspect_name.id IS 'Primary key.';


--
-- Name: COLUMN book_aspect_name.code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_aspect_name.code IS 'Uniquely identifies the code in a language-neutral way.';


--
-- Name: COLUMN book_aspect_name.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_aspect_name.label IS 'Human-readable name for the aspect. Can be internationalized.';


--
-- Name: COLUMN book_aspect_name.description; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_aspect_name.description IS 'Brief description of how the aspect is meant to be used. Values in this column could be also displayed to the user.';


--
-- Name: book_aspect_name_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_aspect_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_aspect_name_id_seq OWNER TO recipe;

--
-- Name: book_aspect_name_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_aspect_name_id_seq OWNED BY book_aspect_name.id;


--
-- Name: book_aspect_name_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_aspect_name_id_seq', 11, true);


--
-- Name: book_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_id_seq OWNER TO recipe;

--
-- Name: book_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_id_seq OWNED BY book.id;


--
-- Name: book_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_id_seq', 20, true);


--
-- Name: book_isbn; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_isbn (
    id integer NOT NULL,
    book_id integer NOT NULL,
    isbn character varying(13) NOT NULL
);


ALTER TABLE recipe_book.book_isbn OWNER TO recipe;

--
-- Name: TABLE book_isbn; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_isbn IS 'ISBN for the given book.';


--
-- Name: COLUMN book_isbn.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_isbn.id IS 'Primary key.';


--
-- Name: COLUMN book_isbn.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_isbn.book_id IS 'Reference to the book that requires an ISBN code.';


--
-- Name: COLUMN book_isbn.isbn; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_isbn.isbn IS 'ISBN book number.';


--
-- Name: book_isbn_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_isbn_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_isbn_id_seq OWNER TO recipe;

--
-- Name: book_isbn_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_isbn_id_seq OWNED BY book_isbn.id;


--
-- Name: book_isbn_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_isbn_id_seq', 1, false);


--
-- Name: book_latex; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_latex (
    id integer NOT NULL,
    snippet_code character varying(32) NOT NULL,
    latex text NOT NULL
);


ALTER TABLE recipe_book.book_latex OWNER TO recipe;

--
-- Name: TABLE book_latex; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_latex IS 'Associates LaTeX code with a book preference.';


--
-- Name: COLUMN book_latex.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_latex.id IS 'Primary key.';


--
-- Name: COLUMN book_latex.latex; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_latex.latex IS 'Source code associated with a configurable preference.';


--
-- Name: book_latex_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_latex_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_latex_id_seq OWNER TO recipe;

--
-- Name: book_latex_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_latex_id_seq OWNED BY book_latex.id;


--
-- Name: book_latex_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_latex_id_seq', 19, true);


--
-- Name: book_photograph; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_photograph (
    id integer NOT NULL,
    book_id integer NOT NULL,
    recipe_id integer,
    book_photograph_category_id integer NOT NULL,
    image_url character varying(2048) NOT NULL
);


ALTER TABLE recipe_book.book_photograph OWNER TO recipe;

--
-- Name: TABLE book_photograph; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_photograph IS 'Describes attributes about a photograph that can be used in the book.';


--
-- Name: COLUMN book_photograph.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_photograph.id IS 'Primary key.';


--
-- Name: COLUMN book_photograph.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_photograph.book_id IS 'The book to which this photograph belongs.';


--
-- Name: COLUMN book_photograph.book_photograph_category_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_photograph.book_photograph_category_id IS 'The type of photograph (e.g., cover page, meal inset, divider page, category section page).';


--
-- Name: COLUMN book_photograph.image_url; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_photograph.image_url IS 'The website address where the image can be retrieved.';


--
-- Name: book_photograph_category; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_photograph_category (
    id integer NOT NULL,
    label character varying NOT NULL,
    description character varying(128) NOT NULL
);


ALTER TABLE recipe_book.book_photograph_category OWNER TO recipe;

--
-- Name: TABLE book_photograph_category; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_photograph_category IS 'Contains a list of categories for book photographs.';


--
-- Name: COLUMN book_photograph_category.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_photograph_category.id IS 'Primary key.';


--
-- Name: COLUMN book_photograph_category.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_photograph_category.label IS 'Text to display to the user.';


--
-- Name: COLUMN book_photograph_category.description; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_photograph_category.description IS 'Brief description regarding how the category is used.';


--
-- Name: book_photograph_category_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_photograph_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_photograph_category_id_seq OWNER TO recipe;

--
-- Name: book_photograph_category_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_photograph_category_id_seq OWNED BY book_photograph_category.id;


--
-- Name: book_photograph_category_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_photograph_category_id_seq', 6, true);


--
-- Name: book_photograph_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_photograph_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_photograph_id_seq OWNER TO recipe;

--
-- Name: book_photograph_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_photograph_id_seq OWNED BY book_photograph.id;


--
-- Name: book_photograph_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_photograph_id_seq', 5, true);


--
-- Name: book_photograph_vw; Type: VIEW; Schema: recipe_book; Owner: recipe
--

CREATE VIEW book_photograph_vw AS
    SELECT rbbp.id, rbbp.book_id, rbbp.recipe_id, rbbp.book_photograph_category_id, rbbp.image_url, rbbpc.label AS photograph_category FROM book_photograph rbbp, book_photograph_category rbbpc WHERE (rbbp.book_photograph_category_id = rbbpc.id);


ALTER TABLE recipe_book.book_photograph_vw OWNER TO recipe;

--
-- Name: book_recipe; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_recipe (
    id integer NOT NULL,
    book_id integer NOT NULL,
    recipe_id integer NOT NULL,
    seq integer DEFAULT 0 NOT NULL
);


ALTER TABLE recipe_book.book_recipe OWNER TO recipe;

--
-- Name: TABLE book_recipe; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_recipe IS 'Associates a book with a recipe.';


--
-- Name: COLUMN book_recipe.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_recipe.id IS 'Primary key.';


--
-- Name: COLUMN book_recipe.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_recipe.book_id IS 'Reference to the book table.';


--
-- Name: COLUMN book_recipe.recipe_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_recipe.recipe_id IS 'Reference to the recipe table.';


--
-- Name: COLUMN book_recipe.seq; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_recipe.seq IS 'Determines the order of recipes in a book.';


--
-- Name: book_recipe_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_recipe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_recipe_id_seq OWNER TO recipe;

--
-- Name: book_recipe_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_recipe_id_seq OWNED BY book_recipe.id;


--
-- Name: book_recipe_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_recipe_id_seq', 1336, true);


--
-- Name: book_snippet; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_snippet (
    id integer NOT NULL,
    code character varying(32) NOT NULL,
    label character varying(32) NOT NULL,
    aspect_code character varying(32) DEFAULT 'COLOUR_SCHEME'::character varying NOT NULL
);


ALTER TABLE recipe_book.book_snippet OWNER TO recipe;

--
-- Name: TABLE book_snippet; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_snippet IS 'Lists the names of book snippets. The snippets are associated with specific aspects.';


--
-- Name: COLUMN book_snippet.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet.id IS 'Primary key.';


--
-- Name: COLUMN book_snippet.code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet.code IS 'Uniquely identifies the code in a language-neutral way.';


--
-- Name: COLUMN book_snippet.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet.label IS 'Human-readable name for the snippet. Can be internationalized.';


--
-- Name: COLUMN book_snippet.aspect_code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet.aspect_code IS 'Associates the book snippet with a book aspect.';


--
-- Name: book_snippet_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_snippet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_snippet_id_seq OWNER TO recipe;

--
-- Name: book_snippet_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_snippet_id_seq OWNED BY book_snippet.id;


--
-- Name: book_snippet_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_snippet_id_seq', 19, true);


--
-- Name: book_snippet_preference; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_snippet_preference (
    id integer NOT NULL,
    book_id integer NOT NULL,
    book_snippet_id integer NOT NULL
);


ALTER TABLE recipe_book.book_snippet_preference OWNER TO recipe;

--
-- Name: TABLE book_snippet_preference; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_snippet_preference IS 'Allows users to override aspects of a book theme by directly referencing a LaTeX snippet. For example, MODERN_SQUARE theme uses the LINUX_LIBERTINE font, but this could be overruled using the PAGELLA_ADVENTOR snippet (belonging to the FONT aspect).';


--
-- Name: COLUMN book_snippet_preference.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet_preference.id IS 'Primary key.';


--
-- Name: COLUMN book_snippet_preference.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet_preference.book_id IS 'Reference to the book that has some of its snippets overruled.';


--
-- Name: COLUMN book_snippet_preference.book_snippet_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_snippet_preference.book_snippet_id IS 'Reference to the snippet that has overruled an aspect from the book''s theme preference.';


--
-- Name: book_snippet_preference_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_snippet_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_snippet_preference_id_seq OWNER TO recipe;

--
-- Name: book_snippet_preference_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_snippet_preference_id_seq OWNED BY book_snippet_preference.id;


--
-- Name: book_snippet_preference_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_snippet_preference_id_seq', 1, false);


--
-- Name: book_spine; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_spine (
    id integer NOT NULL,
    book_id integer NOT NULL,
    label character varying(64) NOT NULL
);


ALTER TABLE recipe_book.book_spine OWNER TO recipe;

--
-- Name: TABLE book_spine; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_spine IS 'Label for the spine of a book.';


--
-- Name: COLUMN book_spine.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_spine.id IS 'Primary key.';


--
-- Name: COLUMN book_spine.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_spine.book_id IS 'Reference to the book that requires a spine label.';


--
-- Name: COLUMN book_spine.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_spine.label IS 'Label for the spine of a book.';


--
-- Name: book_spine_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_spine_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_spine_id_seq OWNER TO recipe;

--
-- Name: book_spine_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_spine_id_seq OWNED BY book_spine.id;


--
-- Name: book_spine_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_spine_id_seq', 1, false);


--
-- Name: book_theme; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_theme (
    id integer NOT NULL,
    theme_code character varying(32) NOT NULL,
    aspect_code character varying(32) NOT NULL,
    snippet_code character varying(32) NOT NULL,
    seq integer DEFAULT 0 NOT NULL
);


ALTER TABLE recipe_book.book_theme OWNER TO recipe;

--
-- Name: TABLE book_theme; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_theme IS 'A book theme is comprised of a number of aspects. The aspects per theme must be unique. That is, a book theme cannot have two COLOUR_SCHEME aspects.';


--
-- Name: COLUMN book_theme.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme.id IS 'Primary key.';


--
-- Name: COLUMN book_theme.theme_code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme.theme_code IS 'Reference to the theme''s name.';


--
-- Name: COLUMN book_theme.aspect_code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme.aspect_code IS 'Reference to the aspect. Each aspect can only appear once in a theme.';


--
-- Name: COLUMN book_theme.snippet_code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme.snippet_code IS 'Indirect reference to the aspect code, via a snippet.';


--
-- Name: COLUMN book_theme.seq; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme.seq IS 'Queries for a book theme should fetch the results in the order denoted by this column.';


--
-- Name: book_theme_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_theme_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_theme_id_seq OWNER TO recipe;

--
-- Name: book_theme_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_theme_id_seq OWNED BY book_theme.id;


--
-- Name: book_theme_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_theme_id_seq', 7, true);


--
-- Name: book_theme_name; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_theme_name (
    id integer NOT NULL,
    code character varying(32) NOT NULL,
    label character varying NOT NULL
);


ALTER TABLE recipe_book.book_theme_name OWNER TO recipe;

--
-- Name: TABLE book_theme_name; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_theme_name IS 'Lists all the book theme names (e.g., maps MODERN_SQUARE to "Modern Square").';


--
-- Name: COLUMN book_theme_name.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_name.id IS 'Primary key.';


--
-- Name: COLUMN book_theme_name.code; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_name.code IS 'Uniquely identifies the theme.';


--
-- Name: COLUMN book_theme_name.label; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_name.label IS 'Textual description of the book.';


--
-- Name: book_theme_name_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_theme_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_theme_name_id_seq OWNER TO recipe;

--
-- Name: book_theme_name_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_theme_name_id_seq OWNED BY book_theme_name.id;


--
-- Name: book_theme_name_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_theme_name_id_seq', 3, true);


--
-- Name: book_theme_preference; Type: TABLE; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE TABLE book_theme_preference (
    id integer NOT NULL,
    book_id integer NOT NULL,
    book_theme_name_id integer NOT NULL
);


ALTER TABLE recipe_book.book_theme_preference OWNER TO recipe;

--
-- Name: TABLE book_theme_preference; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON TABLE book_theme_preference IS 'Associates a book_them with a book.';


--
-- Name: COLUMN book_theme_preference.id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_preference.id IS 'Primary key.';


--
-- Name: COLUMN book_theme_preference.book_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_preference.book_id IS 'Reference to an existing book.';


--
-- Name: COLUMN book_theme_preference.book_theme_name_id; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON COLUMN book_theme_preference.book_theme_name_id IS 'Reference to the theme that should be applied to the book.';


--
-- Name: book_theme_preference_id_seq; Type: SEQUENCE; Schema: recipe_book; Owner: recipe
--

CREATE SEQUENCE book_theme_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_book.book_theme_preference_id_seq OWNER TO recipe;

--
-- Name: book_theme_preference_id_seq; Type: SEQUENCE OWNED BY; Schema: recipe_book; Owner: recipe
--

ALTER SEQUENCE book_theme_preference_id_seq OWNED BY book_theme_preference.id;


--
-- Name: book_theme_preference_id_seq; Type: SEQUENCE SET; Schema: recipe_book; Owner: recipe
--

SELECT pg_catalog.setval('book_theme_preference_id_seq', 1, true);


--
-- Name: book_theme_vw; Type: VIEW; Schema: recipe_book; Owner: recipe
--

CREATE VIEW book_theme_vw AS
    SELECT rbbtn.id AS book_theme_name_id, rbbt.theme_code, rbbt.aspect_code, rbbt.snippet_code, rbbs.label, rbbl.latex FROM book_theme_name rbbtn, book_theme rbbt, book_snippet rbbs, book_latex rbbl WHERE ((((rbbtn.code)::text = (rbbt.theme_code)::text) AND ((rbbt.snippet_code)::text = (rbbs.code)::text)) AND ((rbbl.snippet_code)::text = (rbbs.code)::text)) ORDER BY rbbt.seq;


ALTER TABLE recipe_book.book_theme_vw OWNER TO recipe;

--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book ALTER COLUMN id SET DEFAULT nextval('book_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_account ALTER COLUMN id SET DEFAULT nextval('book_account_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_aspect_name ALTER COLUMN id SET DEFAULT nextval('book_aspect_name_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_isbn ALTER COLUMN id SET DEFAULT nextval('book_isbn_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_latex ALTER COLUMN id SET DEFAULT nextval('book_latex_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_photograph ALTER COLUMN id SET DEFAULT nextval('book_photograph_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_photograph_category ALTER COLUMN id SET DEFAULT nextval('book_photograph_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_recipe ALTER COLUMN id SET DEFAULT nextval('book_recipe_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_snippet ALTER COLUMN id SET DEFAULT nextval('book_snippet_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_snippet_preference ALTER COLUMN id SET DEFAULT nextval('book_snippet_preference_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_spine ALTER COLUMN id SET DEFAULT nextval('book_spine_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_theme ALTER COLUMN id SET DEFAULT nextval('book_theme_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_theme_name ALTER COLUMN id SET DEFAULT nextval('book_theme_name_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE book_theme_preference ALTER COLUMN id SET DEFAULT nextval('book_theme_preference_id_seq'::regclass);


--
-- Data for Name: book; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book VALUES (2, 'Making Brussels Sprouts Speak', '2013-01-26 17:52:00.294164-08', NULL, NULL);
INSERT INTO book VALUES (3, 'Creative Caramel Catastrophes', '2013-01-27 18:45:34.210865-08', NULL, NULL);
INSERT INTO book VALUES (17, 'Book Name', '2013-02-08 19:12:08.247764-08', NULL, NULL);
INSERT INTO book VALUES (18, 'Chicken Gumbo', '2013-02-09 20:07:05.464286-08', NULL, NULL);
INSERT INTO book VALUES (19, 'Most Amazing Recipe Book', '2013-02-10 15:08:03.51472-08', NULL, NULL);
INSERT INTO book VALUES (20, 'Reddit''s Recipes', '2013-02-12 16:24:03.196985-08', NULL, NULL);


--
-- Data for Name: book_account; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_account VALUES (1, 2, 4);
INSERT INTO book_account VALUES (2, 3, 4);
INSERT INTO book_account VALUES (16, 17, 4);
INSERT INTO book_account VALUES (17, 18, 4);
INSERT INTO book_account VALUES (18, 19, 4);
INSERT INTO book_account VALUES (19, 20, 4);


--
-- Data for Name: book_aspect_name; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_aspect_name VALUES (1, 'COLOUR_SCHEME', 'Colour Scheme', 'Sets the primary, secondary, and accent colours.');
INSERT INTO book_aspect_name VALUES (3, 'FONT', 'Font Style', 'Sets the primary and secondary fonts.');
INSERT INTO book_aspect_name VALUES (4, 'LIST_STYLE', 'List Style', 'Sets the appearance for enumerated and bullet lists.');
INSERT INTO book_aspect_name VALUES (7, 'PAGE_LAYOUT', 'Page Layout', 'Changes the arrangement of recipe parts on the page.');
INSERT INTO book_aspect_name VALUES (9, 'PAGE_HEADER', 'Page Header', 'Defines the elements and style at the top of each page.');
INSERT INTO book_aspect_name VALUES (8, 'PAGE_FOOTER', 'Page Footer', 'Defines the elements and style at the bottom of each page.');
INSERT INTO book_aspect_name VALUES (2, 'INSET_PHOTO', 'Inset Photographs', 'Decorates photographs on the same page as the recipe text.');
INSERT INTO book_aspect_name VALUES (10, 'UNCATEGORIZED', 'Uncategorized', 'Miscellaneous LaTeX snippets used by themes.');
INSERT INTO book_aspect_name VALUES (11, 'PDF', 'PDF Settings', 'Options for controlling various PDF settings.');


--
-- Data for Name: book_isbn; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Data for Name: book_latex; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_latex VALUES (7, 'SANS_SERIF', '\renewcommand{\recipefamily}{\sffamily}
\renewcommand{\headingfamily}{\sffamily}
\renewcommand{\familydefault}{\rmdefault}
');
INSERT INTO book_latex VALUES (8, 'SERIF', '\renewcommand{\recipefamily}{\sffamily}
\renewcommand{\headingfamily}{\sffamily}
\renewcommand{\familydefault}{\sfdefault}
');
INSERT INTO book_latex VALUES (9, 'SERIF_SANS', '\renewcommand{\recipefamily}{\rmfamily}
\renewcommand{\headingfamily}{\rmfamily}
\renewcommand{\familydefault}{\sfdefault}
');
INSERT INTO book_latex VALUES (10, 'SERIF', '\renewcommand{\recipefamily}{\rmfamily}
\renewcommand{\headingfamily}{\rmfamily}
\renewcommand{\familydefault}{\rmdefault}
');
INSERT INTO book_latex VALUES (12, 'CIRCLES', '\renewcommand{\headingmarker}{\raisebox{1pt}{\small\CIRCLE}}
\renewcommand{\itemmarker}{\color{\secondarycolor}{\CIRCLE}}
\renewcommand{\inlinelistmarker}{\raisebox{1pt}{\makebox[1.5em][c]{\color{\secondarycolor}\scriptsize\CIRCLE}}}
\renewcommand{\enumeratemarker}[1]{\circledNum{#1}}
');
INSERT INTO book_latex VALUES (13, 'SQUARES', '\renewcommand{\headingmarker}{\tikz[baseline=-3pt]\node[diamond,draw=none,inner sep=2.25pt,fill=\headingcolor]{};}
\renewcommand{\itemmarker}{\textcolor{\secondarycolor}{\scriptsize$\blacksquare$}}
\renewcommand{\inlinelistmarker}{\makebox[1.5em][c]{\color{\secondarycolor}\tiny$\blacksquare$}}
\renewcommand{\enumeratemarker}[1]{\framedNum{#1}}
');
INSERT INTO book_latex VALUES (14, 'ONE_COLUMN_SPREAD_REVERSE', '\renewenvironment{recipe}[1]{%
   \clearpage%
   \renewcommand{\recipetitle}{#1}
   \color{\maintextcolor}%
}{%
   \clearpage%
}

\renewcommand{\startinstructions}{}

\compactingredients
\compactequipment
%\compactinstructions

%\renewcommand{\startinstructions}{\columnbreak}
\renewcommand{\photo}[1]{%
%       \ThisCenterWallPaper{1}{#1}\cleardoublepage
        \fullpagephoto{#1}\cleardoublepage
        \section{\recipetitle}
');
INSERT INTO book_latex VALUES (1, 'CIURLIONIS', '% http://www.colourlovers.com/palette/2634294/APC393_%C4%8Ciurlionis
\definecolor[named]{skin}{HTML}{F2B166}
\definecolor[named]{nectar}{HTML}{EDE2C2}
\definecolor[named]{ifiwerestronger}{HTML}{D2B490}
\definecolor[named]{p}{HTML}{AB8369}
\definecolor[named]{suspect}{HTML}{755348}
\definecolor[named]{sky}{HTML}{1982D1}

\renewcommand{\primarycolor}{p}
\renewcommand{\secondarycolor}{ifiwerestronger}
\renewcommand{\tertiarycolor}{nectar}
\renewcommand{\darkhighlight}{suspect}
\renewcommand{\brighthighlight}{skin}
\renewcommand{\primarycolorcomplement}{black}
\renewcommand{\secondarycolorcomplement}{black}
\renewcommand{\tertiarycolorcomplement}{black}
\renewcommand{\externallinkcolor}{sky}
');
INSERT INTO book_latex VALUES (4, 'AUGIE', '% http://www.dafont.com/augie.font
\renewcommand{\rmdefault}{augie}
\renewcommand{\sfdefault}{augie}
\small
');
INSERT INTO book_latex VALUES (5, 'LINUX_LIBERTINE', '% Serif = Linux Libertine; Sans serif = Linux Biolinum
\renewcommand{\recipefamily}{\sffamily}
\renewcommand{\headingfamily}{\sffamily}
\renewcommand{\familydefault}{\rmdefault}

% http://www.linuxlibertine.org/
\usepackage[lf]{libertine}
');
INSERT INTO book_latex VALUES (6, 'PAGELLA_ADVENTOR', '% Serif = TeX Gyre Pagella (Palatino replacement)
% Sans serif = TeX Gyre Adventor (Avant Garde Gothic replacement)
% http://www.fontsquirrel.com/fonts/TeX-Gyre-Pagella
\usepackage{tgpagella}

% http://www.fontsquirrel.com/fonts/TeX-Gyre-Adventor
\usepackage[scale=.89]{tgadventor}
');
INSERT INTO book_latex VALUES (11, 'TERMES_HEROS', '% Sans serif = TeX Gyer Heros (Helvetica replacement)
% http://www.fontsquirrel.com/fonts/TeX-Gyre-Heros
\usepackage[scale=.89]{tgheros}

% Serif = Tex Gyre Termes (Times Roman replacement)
% http://www.fontsquirrel.com/fonts/TeX-Gyre-Termes
\usepackage{tgtermes}
');
INSERT INTO book_latex VALUES (2, 'CHOCOBERRY', '% http://www.colourlovers.com/palette/2634592/See_2
\definecolor[named]{foggy}{HTML}{F5F6CE}
\definecolor[named]{RosyAlpaca}{HTML}{DBB9A1}
\definecolor[named]{LoveMeFour}{HTML}{A45F62}
\definecolor[named]{one}{HTML}{71574B}
\definecolor[named]{PlataMate}{HTML}{9A957A}
\definecolor[named]{sky}{HTML}{1921D1}

\renewcommand{\primarycolor}{PlataMate}
\renewcommand{\secondarycolor}{RosyAlpaca!50}
\renewcommand{\tertiarycolor}{foggy}
\renewcommand{\primarycolorcomplement}{white}
\renewcommand{\secondarycolorcomplement}{black}
\renewcommand{\tertiarycolorcomplement}{black}
\renewcommand{\darkhighlight}{one}
\renewcommand{\brighthighlight}{LoveMeFour}
\renewcommand{\highlightcolor}{\brighthighlight}
\renewcommand{\externallinkcolor}{sky}');
INSERT INTO book_latex VALUES (3, 'SWEET_CANDY', '% http://www.colourlovers.com/palette/848743/(%E2%97%95_%E2%80%9D_%E2%97%95)
\definecolor[named]{PartyConfetti}{HTML}{490A3D}
\definecolor[named]{SugarHeartsYou}{HTML}{BD1550}
\definecolor[named]{SugarCocktail}{HTML}{E97F02}
\definecolor[named]{BurstsOfEuphoria}{HTML}{F8CA00}
\definecolor[named]{HappyBalloon}{HTML}{8A9B0F}
\definecolor[named]{sky}{HTML}{1921D1}

\renewcommand{\primarycolor}{SugarHeartsYou}
\renewcommand{\secondarycolor}{SugarCocktail}
\renewcommand{\tertiarycolor}{BurstsOfEuphoria}
\renewcommand{\primarycolorcomplement}{white}
\renewcommand{\secondarycolorcomplement}{white}
\renewcommand{\tertiarycolorcomplement}{black}
\renewcommand{\darkhighlight}{PartyConfetti}
\renewcommand{\brighthighlight}{HappyBalloon}
\renewcommand{\highlightcolor}{\brighthighlight}
\renewcommand{\externallinkcolor}{sky}');
INSERT INTO book_latex VALUES (17, 'TWO_COLUMN_TWO_SIDE', '\renewenvironment{recipe}[1]{%
   \clearpage%
   \color{\maintextcolor}%
   \raggedcolumns
   \checkoddpage\strictpagecheck\ifoddpage\RLmulticolcolumns\else\LRmulticolcolumns\fi
   \begin{multicols*}{2}[\section{#1}]%
}{%
   \end{multicols*}%
}

\renewcommand{\startinstructions}{\columnbreak}

\renewcommand{\photo}[1]{\insetphoto{#1}}

\normalingredients
');
INSERT INTO book_latex VALUES (16, 'UNDEFINED_NOTEBOOK', '% Lian Tze LIM
% http://liantze.penguinattack.org/
% 16 January 2013

\providecommand{\handUnderline}[1]{%
  \settowidth{\tmplength}{#1}%
  #1\vskip-.8em
  \tikz[decoration={random steps,segment length=2mm,amplitude=1pt}]\draw[decorate,line width=.15ex,line cap=round] (0,0) -- (\tmplength,0);\par
}

\providecommand{\scribbledHeading}[1]{%
  \handUnderline{\headingmarker\ #1}
}

\renewcommand{\recipesize}{\Huge}
\renewcommand{\recipestyle}[1]{\centering\handUnderline{#1}}
\renewcommand{\headingsize}{\large}
\renewcommand{\headingstyle}{\scribbledHeading}

\setcolsepandrule{3em}{6pt}
\renewcommand{\columnseprulecolor}{\color{\tertiarycolor}}

\renewcommand{\recipesize}{\Huge}
\renewcommand{\headingsize}{\Large}

\renewcommand{\headingmarker}{\textcolor{\headingcolor}{\&}}
\renewcommand{\itemmarker}{\textcolor{\secondarycolor}{*}}
\renewcommand{\enumeratemarker}[1]{\textcolor{\secondarycolor}{\# #1.}}
\renewcommand{\inlinelistmarker}{\makebox[1.5em][c]{\textcolor{\secondarycolor}{\textbullet}}}

\renewcommand{\ingredientsstyle}{%
  nosep,%
  label=\itemmarker,%
  leftmargin=*
}

\renewcommand\descriptionlabel[1]{%
  \hspace\labelsep
  \normalfont\textcolor{\darkhighlight}{#1:}}

\renewcommand{\equipmentstyle}{%
  mode=unboxed,%
  leftmargin=*,%
  itemjoin=\inlinelistmarker,
  noitemsep
}

\renewcommand{\instructionsstyle}{%
  leftmargin=*,%
  itemsep=1pt,%
  label={\enumeratemarker{\arabic*}}
}

%\pgfmathsetseed{\pdfuniformdeviate 10000000}
%\pgfmathsetseed{1}
\providecommand{\rotatepic}[1]{%
\noindent\raggedright%
\begin{tikzpicture}
\node[rotate={rand*4.0},inner sep=1em,draw=gray!60,fill=white, blur shadow={shadow blur steps=5,shadow xshift=1ex,shadow yshift=-1ex,shadow opacity=20,shadow blur radius=1ex}]{\includegraphics[width=.85\linewidth]{#1}};
\end{tikzpicture}
}

\renewcommand{\insetphoto}[1]{\rotatepic{#1}\par\bigskip}
');
INSERT INTO book_latex VALUES (18, 'TRUE_HYPERLINKS', '% PDF hyperlink style and configuration
\hypersetup{
  % Show bookmarks bar?
  bookmarks=true,
  % Non-Latin characters in Acrobat''s bookmarks
  unicode=true,
  % Show Acrobat''s toolbar?
  pdftoolbar=true,
  % Show Acrobat''s menu?
  pdfmenubar=true,
  % Window fit to page when opened
  pdffitwindow=false,
  % Fit the width of the page to the window
  pdfstartview={FitH},
  % TODO: Title
  %pdftitle={Recipe Title},
  % TODO: author
  %pdfauthor={Recipe Book Author},
  % subject of the document
  pdfsubject={Recipes},
  % Creator of the document
  pdfcreator={LianTze Lim},
  % Producer of the document
  pdfproducer={pdflatex},
  % TODO: List of keywords
  pdfkeywords={recipe},
  % Links in new window
  pdfnewwindow=true,
  % false: boxed links; true: colored links
  colorlinks=true,
  % Color of internal links
  linkcolor=\primarycolor,
  % Color of links to bibliography
  citecolor=black,
  % Color of file links
  filecolor=black,
  % Color of external links
  urlcolor=\externallinkcolor,
  % No border around links
  pdfborder={0 0 0}
}
');
INSERT INTO book_latex VALUES (15, 'UNDEFINED_MODERN', '% Lian Tze LIM
% http://liantze.penguinattack.org/
% 16 January 2013
%
% Layout inspiration: "100 Recipes from Japanese Cooking"
% Authors: Hata Koichiro and Kondo Kazuki
% Publisher: Kodansha Books 
% ISBN: 978-4770020796

\providecommand{\fullruled}[1]{#1\vskip-3pt\rule{\linewidth}{1pt}}
\providecommand{\halfruled}[1]{\headingmarker\ #1\vskip-6pt\rule{.5\linewidth}{.4pt}\vskip-3pt}

\setcolsepandrule{3em}{6pt}
\renewcommand{\columnseprulecolor}{\color{\tertiarycolor}}

\chapterstyle{bianchi}
\patchcommand{\printchaptertitle}{\color{\recipecolor}}{}

\renewcommand{\recipesize}{\Huge}
\renewcommand{\recipestyle}{\bfseries\itshape\checkoddpage\strictpagecheck\ifoddpage\raggedleft\else\raggedright\fi\fullruled}
\renewcommand{\headingsize}{\large}
\renewcommand{\headingstyle}{\bfseries\halfruled}

\renewcommand{\ovenstyle}{\itshape\par}

\renewcommand{\ingredientsstyle}{%
  nosep,%
  label=\itemmarker,%
  leftmargin=*
}

\renewcommand{\equipmentstyle}{%
  mode=unboxed,%
  leftmargin=*,%
  itemjoin=\inlinelistmarker,
  noitemsep
}

\renewcommand{\instructionsstyle}{%
  leftmargin=*,%
  itemsep=1pt,%
  label={\enumeratemarker{\arabic*}}
}

\renewcommand{\insetphoto}[1]{%
   \includegraphics[width=\linewidth]{#1}\par\bigskip
}');
INSERT INTO book_latex VALUES (19, 'FRAMED_FOOTER', '\makeevenhead{headings}{}{}{}
\makeoddhead{headings}{}{}{}
\makeevenfoot{headings}{\framedPage}{}{}
\makeoddfoot{headings}{}{}{\framedPage}
\makeevenfoot{plain}{\framedPage}{}{}
\makeoddfoot{plain}{}{}{\framedPage}');


--
-- Data for Name: book_photograph; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_photograph VALUES (1, 20, NULL, 3, 'http://i.imgur.com/YlsIHBT.png');
INSERT INTO book_photograph VALUES (2, 20, 2, 5, 'http://static.flickr.com/7110/6878257218_19a31691df_h.jpg');
INSERT INTO book_photograph VALUES (3, 20, 3, 5, 'http://static.flickr.com/2399/2231161820_cee7e9d100_b.jpg');
INSERT INTO book_photograph VALUES (4, 20, 1, 5, 'http://static.flickr.com/3005/2950536388_c7795e9af9_b.jpg');
INSERT INTO book_photograph VALUES (5, 20, 4, 5, 'http://static.flickr.com/2794/4135509624_df3329d4fb_z.jpg?zz=1');


--
-- Data for Name: book_photograph_category; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_photograph_category VALUES (1, 'one-page', 'Photograph is on one page, opposite its recipe.');
INSERT INTO book_photograph_category VALUES (3, 'front-cover', 'Photograph for the book''s front cover.');
INSERT INTO book_photograph_category VALUES (4, 'back-cover', 'Photograph for the book''s back cover.');
INSERT INTO book_photograph_category VALUES (2, 'two-page', 'Photograph spans the background for the left and right pages.');
INSERT INTO book_photograph_category VALUES (5, 'recipe-inset', 'Photograph and recipe on the same page.');
INSERT INTO book_photograph_category VALUES (6, 'section-page', 'Photograph splits the categories (e.g., soup, meat, and lunch).');


--
-- Data for Name: book_recipe; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_recipe VALUES (1321, 17, 1, 1);
INSERT INTO book_recipe VALUES (1322, 17, 2, 2);
INSERT INTO book_recipe VALUES (1323, 17, 3, 3);
INSERT INTO book_recipe VALUES (1324, 17, 4, 4);
INSERT INTO book_recipe VALUES (1273, 19, 1, 1);
INSERT INTO book_recipe VALUES (1274, 19, 2, 2);
INSERT INTO book_recipe VALUES (1275, 19, 3, 3);
INSERT INTO book_recipe VALUES (1276, 19, 4, 4);
INSERT INTO book_recipe VALUES (1333, 20, 1, 1);
INSERT INTO book_recipe VALUES (1334, 20, 2, 2);
INSERT INTO book_recipe VALUES (1335, 20, 3, 3);
INSERT INTO book_recipe VALUES (1336, 20, 4, 4);


--
-- Data for Name: book_snippet; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_snippet VALUES (1, 'CIURLIONIS', 'Čiurlionis', 'COLOUR_SCHEME');
INSERT INTO book_snippet VALUES (2, 'CHOCOBERRY', 'Chocoberry', 'COLOUR_SCHEME');
INSERT INTO book_snippet VALUES (3, 'SWEET_CANDY', 'Sweet Candy', 'COLOUR_SCHEME');
INSERT INTO book_snippet VALUES (5, 'LINUX_LIBERTINE', 'Penguin Liberty', 'FONT');
INSERT INTO book_snippet VALUES (6, 'PAGELLA_ADVENTOR', 'Leafy Adventure', 'FONT');
INSERT INTO book_snippet VALUES (4, 'AUGIE', 'Third Grade', 'FONT');
INSERT INTO book_snippet VALUES (7, 'SANS_SERIF', 'Sans over Serif', 'FONT');
INSERT INTO book_snippet VALUES (8, 'SANS', 'Sans Alone', 'FONT');
INSERT INTO book_snippet VALUES (9, 'SERIF_SANS', 'Serif over Sans', 'FONT');
INSERT INTO book_snippet VALUES (10, 'SERIF', 'Serif Alone', 'FONT');
INSERT INTO book_snippet VALUES (11, 'TERMES_HEROS', 'Old Time Héros', 'FONT');
INSERT INTO book_snippet VALUES (12, 'CIRCLES', 'Circles', 'LIST_STYLE');
INSERT INTO book_snippet VALUES (13, 'SQUARES', 'Squares', 'LIST_STYLE');
INSERT INTO book_snippet VALUES (14, 'ONE_COLUMN_SPREAD_REVERSE', 'Full Page Pictures', 'PAGE_LAYOUT');
INSERT INTO book_snippet VALUES (15, 'FRAMED_FOOTER', 'Framed Page Number', 'PAGE_FOOTER');
INSERT INTO book_snippet VALUES (16, 'UNDEFINED_MODERN', 'Undefined Modern', 'UNCATEGORIZED');
INSERT INTO book_snippet VALUES (17, 'UNDEFINED_NOTEBOOK', 'Undefined Notebook', 'UNCATEGORIZED');
INSERT INTO book_snippet VALUES (18, 'TWO_COLUMN_TWO_SIDE', 'Half and Half', 'PAGE_LAYOUT');
INSERT INTO book_snippet VALUES (19, 'TRUE_HYPERLINKS', 'Regular Hyperlink Style', 'PDF');


--
-- Data for Name: book_snippet_preference; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Data for Name: book_spine; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--



--
-- Data for Name: book_theme; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_theme VALUES (1, 'MODERN_SQUARE', 'FONT', 'LINUX_LIBERTINE', 1);
INSERT INTO book_theme VALUES (2, 'MODERN_SQUARE', 'COLOUR_SCHEME', 'CHOCOBERRY', 2);
INSERT INTO book_theme VALUES (3, 'MODERN_SQUARE', 'LIST_STYLE', 'SQUARES', 3);
INSERT INTO book_theme VALUES (4, 'MODERN_SQUARE', 'PAGE_LAYOUT', 'TWO_COLUMN_TWO_SIDE', 4);
INSERT INTO book_theme VALUES (5, 'MODERN_SQUARE', 'PAGE_FOOTER', 'FRAMED_FOOTER', 5);
INSERT INTO book_theme VALUES (6, 'MODERN_SQUARE', 'UNCATEGORIZED', 'UNDEFINED_MODERN', 6);
INSERT INTO book_theme VALUES (7, 'MODERN_SQUARE', 'PDF', 'TRUE_HYPERLINKS', 7);


--
-- Data for Name: book_theme_name; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_theme_name VALUES (1, 'MODERN_SQUARE', 'Modern Square');
INSERT INTO book_theme_name VALUES (2, 'MODERN_CIRCLE', 'Modern Circle');
INSERT INTO book_theme_name VALUES (3, 'NOTEBOOK', 'Notebook');


--
-- Data for Name: book_theme_preference; Type: TABLE DATA; Schema: recipe_book; Owner: recipe
--

INSERT INTO book_theme_preference VALUES (1, 2, 1);


--
-- Name: pk_book; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book
    ADD CONSTRAINT pk_book PRIMARY KEY (id);


--
-- Name: pk_book_account; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_account
    ADD CONSTRAINT pk_book_account PRIMARY KEY (id);


--
-- Name: pk_book_aspect_name; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_aspect_name
    ADD CONSTRAINT pk_book_aspect_name PRIMARY KEY (id);


--
-- Name: pk_book_latex; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_latex
    ADD CONSTRAINT pk_book_latex PRIMARY KEY (id);


--
-- Name: pk_book_photograph; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_photograph
    ADD CONSTRAINT pk_book_photograph PRIMARY KEY (id);


--
-- Name: pk_book_photograph_category; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_photograph_category
    ADD CONSTRAINT pk_book_photograph_category PRIMARY KEY (id);


--
-- Name: pk_book_recipe; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_recipe
    ADD CONSTRAINT pk_book_recipe PRIMARY KEY (id);


--
-- Name: pk_book_snippet; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_snippet
    ADD CONSTRAINT pk_book_snippet PRIMARY KEY (id);


--
-- Name: pk_book_snippet_preference; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_snippet_preference
    ADD CONSTRAINT pk_book_snippet_preference PRIMARY KEY (id);


--
-- Name: pk_book_theme; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme
    ADD CONSTRAINT pk_book_theme PRIMARY KEY (id);


--
-- Name: pk_book_theme_name; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme_name
    ADD CONSTRAINT pk_book_theme_name PRIMARY KEY (id);


--
-- Name: pk_book_theme_preference; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme_preference
    ADD CONSTRAINT pk_book_theme_preference PRIMARY KEY (id);


--
-- Name: uk_book_aspect_code; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_aspect_name
    ADD CONSTRAINT uk_book_aspect_code UNIQUE (code);


--
-- Name: uk_book_aspect_label; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_aspect_name
    ADD CONSTRAINT uk_book_aspect_label UNIQUE (label);


--
-- Name: uk_book_photograph; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_photograph
    ADD CONSTRAINT uk_book_photograph UNIQUE (book_id, recipe_id, book_photograph_category_id);


--
-- Name: CONSTRAINT uk_book_photograph ON book_photograph; Type: COMMENT; Schema: recipe_book; Owner: recipe
--

COMMENT ON CONSTRAINT uk_book_photograph ON book_photograph IS 'Ensures no two photograph categories are assigned to the same part of a book.';


--
-- Name: uk_book_snippet_code; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_snippet
    ADD CONSTRAINT uk_book_snippet_code UNIQUE (code);


--
-- Name: uk_book_snippet_label; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_snippet
    ADD CONSTRAINT uk_book_snippet_label UNIQUE (label);


--
-- Name: uk_book_theme_name_code; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme_name
    ADD CONSTRAINT uk_book_theme_name_code UNIQUE (code);


--
-- Name: uk_book_theme_preference_book; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme_preference
    ADD CONSTRAINT uk_book_theme_preference_book UNIQUE (book_id, book_theme_name_id);


--
-- Name: uk_book_theme_theme_code_aspect_code; Type: CONSTRAINT; Schema: recipe_book; Owner: recipe; Tablespace: 
--

ALTER TABLE ONLY book_theme
    ADD CONSTRAINT uk_book_theme_theme_code_aspect_code UNIQUE (theme_code, aspect_code);


--
-- Name: idx_book_account; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_account ON book_account USING btree (book_id);


--
-- Name: idx_book_account_0; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_account_0 ON book_account USING btree (account_id);


--
-- Name: idx_book_latex; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_latex ON book_latex USING btree (snippet_code);


--
-- Name: idx_book_photograph; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_photograph ON book_photograph USING btree (book_id);


--
-- Name: idx_book_photograph_0; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_photograph_0 ON book_photograph USING btree (book_photograph_category_id);


--
-- Name: idx_book_recipe; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_recipe ON book_recipe USING btree (recipe_id);


--
-- Name: idx_book_recipe_0; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_recipe_0 ON book_recipe USING btree (book_id);


--
-- Name: idx_book_theme; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_theme ON book_theme USING btree (theme_code);


--
-- Name: idx_book_theme_0; Type: INDEX; Schema: recipe_book; Owner: recipe; Tablespace: 
--

CREATE INDEX idx_book_theme_0 ON book_theme USING btree (snippet_code);


--
-- Name: fk_book_account_account_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_account
    ADD CONSTRAINT fk_book_account_account_id FOREIGN KEY (account_id) REFERENCES recipe.account(id) ON DELETE CASCADE;


--
-- Name: fk_book_account_book_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_account
    ADD CONSTRAINT fk_book_account_book_id FOREIGN KEY (book_id) REFERENCES book(id) ON DELETE CASCADE;


--
-- Name: fk_book_latex_book_snippet; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_latex
    ADD CONSTRAINT fk_book_latex_book_snippet FOREIGN KEY (snippet_code) REFERENCES book_snippet(code) ON DELETE CASCADE;


--
-- Name: fk_book_photograph_book_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_photograph
    ADD CONSTRAINT fk_book_photograph_book_id FOREIGN KEY (book_id) REFERENCES book(id) ON DELETE CASCADE;


--
-- Name: fk_book_photograph_book_photograph_category_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_photograph
    ADD CONSTRAINT fk_book_photograph_book_photograph_category_id FOREIGN KEY (book_photograph_category_id) REFERENCES book_photograph_category(id) ON DELETE CASCADE;


--
-- Name: fk_book_recipe_book_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_recipe
    ADD CONSTRAINT fk_book_recipe_book_id FOREIGN KEY (book_id) REFERENCES book(id) ON DELETE CASCADE;


--
-- Name: fk_book_recipe_recipe_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_recipe
    ADD CONSTRAINT fk_book_recipe_recipe_id FOREIGN KEY (recipe_id) REFERENCES recipe.recipe(id) ON DELETE CASCADE;


--
-- Name: fk_book_snippet_code_aspect_code; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_snippet
    ADD CONSTRAINT fk_book_snippet_code_aspect_code FOREIGN KEY (aspect_code) REFERENCES book_aspect_name(code);


--
-- Name: fk_book_snippet_preference_book_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_snippet_preference
    ADD CONSTRAINT fk_book_snippet_preference_book_id FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: fk_book_snippet_preference_book_snippet_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_snippet_preference
    ADD CONSTRAINT fk_book_snippet_preference_book_snippet_id FOREIGN KEY (book_snippet_id) REFERENCES book_snippet(id);


--
-- Name: fk_book_theme_book_aspect_name; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme
    ADD CONSTRAINT fk_book_theme_book_aspect_name FOREIGN KEY (aspect_code) REFERENCES book_aspect_name(code) ON DELETE CASCADE;


--
-- Name: fk_book_theme_book_snippet; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme
    ADD CONSTRAINT fk_book_theme_book_snippet FOREIGN KEY (snippet_code) REFERENCES book_snippet(code) ON DELETE CASCADE;


--
-- Name: fk_book_theme_book_theme_name; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme
    ADD CONSTRAINT fk_book_theme_book_theme_name FOREIGN KEY (theme_code) REFERENCES book_theme_name(code) ON DELETE CASCADE;


--
-- Name: fk_book_theme_preference_book; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme_preference
    ADD CONSTRAINT fk_book_theme_preference_book FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: fk_book_theme_preference_book_theme_name_id; Type: FK CONSTRAINT; Schema: recipe_book; Owner: recipe
--

ALTER TABLE ONLY book_theme_preference
    ADD CONSTRAINT fk_book_theme_preference_book_theme_name_id FOREIGN KEY (book_theme_name_id) REFERENCES book_theme_name(id);


--
-- PostgreSQL database dump complete
--

