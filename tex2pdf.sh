#!/bin/bash

function usage() {
  echo "$(basename "$0") [-h] [-l path] -f filename

where:
  -h  Show options
  -l  Path to LaTeX bin directory (/usr/local/texlive/2013/bin/x86_64-linux)
  -f  TeX file to convert to a PDF"
}

OPTIND=1
LOG=/tmp/latex/tex2pdf.log
latex=/usr/local/texlive/2013/bin/x86_64-linux

while getopts 'h?lf:' opt; do
  case "$opt" in
    h|\?) usage; exit 0;;
    l) latex=$OPTARG;;
    f) filename=$OPTARG;;
  esac
done
shift $((OPTIND - 1))

if [ -z $filename ]; then
  usage
  echo
  echo "ERROR: Use '-f filename.tex' to specify a file to convert."
  exit 1
fi

LATEX_OPS="-interaction=batchmode"
INDEX_OPS="-q"

$latex/pdflatex $LATEX_OPS -draftmode $filename
$latex/texindy $INDEX_OPS -M flatidx flatingred.idx
$latex/texindy $INDEX_OPS -M deepidx catingred.idx
$latex/pdflatex $LATEX_OPS -draftmode $filename
$latex/pdflatex $LATEX_OPS $filename

