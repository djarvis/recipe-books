﻿-- DROP VIEW recipe_book.book_photograph_category_vw;

CREATE OR REPLACE VIEW recipe_book.book_photograph_category_vw AS
SELECT
  bpc.id,
  bpc.label,
  bpc.description
FROM
  recipe_book.book_photograph_category bpc
WHERE
  bpc.classification = 'RECIPE'
ORDER BY
  bpc.seq;