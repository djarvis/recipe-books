﻿-- DROP VIEW recipe_book.book_theme_vw;

CREATE OR REPLACE VIEW recipe_book.book_theme_vw AS
SELECT 
  rbbtn.id AS book_theme_name_id,
  rbbt.theme_code,
  rbbt.aspect_code,
  rbbt.snippet_code,
  rbbs.label,
  rbbl.latex
FROM 
  recipe_book.book_theme_name rbbtn,
  recipe_book.book_theme rbbt,
  recipe_book.book_snippet rbbs,
  recipe_book.book_latex rbbl
WHERE 
  rbbtn.code = rbbt.theme_code AND
  rbbt.snippet_code = rbbs.code AND
  rbbl.snippet_code = rbbs.code
ORDER BY 
  rbbt.seq;
