% Jan 14 2013
% Lian Tze LIM (liantze@gmail.com)
% Customisable recipe books

\ProvidesClass{recipe-book}[2013/01/14]

\RequirePackage{etoolbox}

\newtoggle{print}\togglefalse{print}
\newtoggle{clearpn}\toggletrue{clearpn}
\newtoggle{inlinecitations}\togglefalse{inlinecitations}
\newtoggle{footercitations}\togglefalse{footercitations}
\newtoggle{endcitations}\toggletrue{endcitations}
\newtoggle{alloriginal}\toggletrue{alloriginal}
\DeclareOption{print}{\toggletrue{print}}
\DeclareOption{fullpagefooter}{\togglefalse{clearpn}}
\DeclareOption{inlinecitations}{\togglefalse{endcitations}\togglefalse{footercitations}\toggletrue{inlinecitations}}
\DeclareOption{footercitations}{\togglefalse{endcitations}\toggletrue{footercitations}\togglefalse{inlinecitations}}
\DeclareOption{endcitations}{\toggletrue{endcitations}\togglefalse{footercitations}\togglefalse{inlinecitations}}
\ProcessOptions\relax

\iftoggle{print}{%
  \LoadClass[twoside,11pt]{memoir}
  \setstocksize{10in}{8in}
  \settrimmedsize{\stockheight}{\stockwidth}{*}
  \setlrmarginsandblock{1.25in}{.75in}{*}
  \setulmarginsandblock{.75in}{1in}{*}
}{%
  \LoadClass[oneside,12pt]{memoir}
  \setstocksize{11in}{8.5in}
  \settrimmedsize{\stockheight}{\stockwidth}{*}
  \setlrmarginsandblock{.75in}{.75in}{*}
  \setulmarginsandblock{.75in}{1in}{*}
}
\setlength{\footskip}{5em}
\setlength{\parindent}{0pt}
\checkandfixthelayout

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{mathpazo}
\RequirePackage{avant}
\RequirePackage{needspace}
\RequirePackage{ccicons}

%%%% Load packages here
\RequirePackage{nicefrac}
\RequirePackage{graphicx}
\RequirePackage[xcolornames,svgnames,dvipsnames,rgb]{xcolor}
\RequirePackage[inline]{enumitem}
\RequirePackage{stringstrings}
\RequirePackage{textcomp,wasysym,marvosym}
\RequirePackage{amssymb}
\RequirePackage{tikz}
\RequirePackage{arrayjobx}
\usetikzlibrary{shapes,decorations,decorations.pathmorphing}
\usetikzlibrary{shadows.blur}
\RequirePackage{endnotes}
\RequirePackage{afterpage}
\RequirePackage{multicol}
\RequirePackage{mccolaction}
\def\columnbreakunlesslast{%
  \docolaction{\typeout{in first column}\inlastfalse}%
              {\typeout{in middle column}\inlasttrue}%
              {\typeout{in last column}\inlasttrue}%
  \unless \ifinlast \columnbreak \fi
}

%% From http://tex.stackexchange.com/a/128021/2148
%% Prevent empty images from breaking the wallpaper package.
\ifdefined\HCode
\else
\newif\ifinlast
  \RequirePackage{wallpaper}
\fi

\RequirePackage{pdflscape}
\RequirePackage{wrapfig}
\RequirePackage{filecontents}
\RequirePackage{microtype}
\RequirePackage{bookmark}
\RequirePackage{relsize}

%%%% I18N
\RequirePackage{translator}
\usedictionary{translation}

% From http://tex.stackexchange.com/a/9266/226
% to fix potential problems with opacity, colourspace
% in PDF
\pdfpageattr {/Group << /S /Transparency /I true /CS /DeviceRGB>>}

%% placeholders
\newcommand{\recipetitle}{}
\providelength{\tmplength}
\providelength{\afterinsetphotoskip}
\setlength{\afterinsetphotoskip}{1em}
\def\@frontcoverphoto{}

% keys for recipe sources
\pgfkeys{
 /recipe/.is family, /recipe,
 default/.style={author=, authorurl=, license=, licenseurl=},
 author/.store in = \r@creditauthor,
 authorurl/.store in = \r@authorurl,
 license/.store in = \r@license,
 licenseurl/.store in = \r@licenseurl,
}

% keys for photos
\pgfkeys{
 /photo/.is family, /photo,
 default/.style={width=\linewidth, embed=inset,%
 author=, authorurl=, license=, licenseurl=},
 width/.store in = \@photowidth,
 author/.store in = \p@creditauthor,
 authorurl/.store in = \p@authorurl,
 license/.store in = \p@license,
 licenseurl/.store in = \p@licenseurl,
 embed/.style = {embeds/#1/.get = \p@embed},
 embeds/.cd,
  inset/.initial = inset,
  fullpage/.initial = fullpage,
  twopagespread/.initial = twopagespread,
}

\gdef\@recipecredittext{}
\gdef\@recipelicensetext{}
\gdef\@photocredittext{}
\gdef\@photolicensetext{}
\gdef\citationtext{}

\newcommand{\recipecredit}[2][]{%
  \global\edef\@recipecredittext{%
    \ifblank{#1}{#2}{\noexpand\href{#1}{#2}}}
}
\newcommand{\recipelicense}[2][]{%
  \global\edef\@recipelicensetext{, %
    \ifblank{#1}{#2}{\noexpand\href{#1}{#2}}}
}
\newcommand{\photocredit}[2][]{%
  \global\edef\@photocredittext{%
    \ifblank{#1}{#2}{\noexpand\href{#1}{#2}}}
}
\newcommand{\photolicense}[2][]{%
  \global\edef\@photolicensetext{, %
    \ifblank{#1}{#2}{\noexpand\href{#1}{#2}}}
}

\newcommand{\parserecipecitation}[1]{%
  \gdef\r@creditauthor{}
  \gdef\r@authorurl{}
  \gdef\r@license{}
  \gdef\r@licenseurl{}
  \gdef\@recipecredittext{}
  \gdef\@recipelicensetext{}
  \pgfkeys{/recipe, default, #1}
  \ifdefempty{\r@creditauthor}{}{%
    \global\togglefalse{alloriginal}%
    \recipecredit[\ifdefempty{\r@authorurl}{}{\r@authorurl}]{\r@creditauthor}}
    \ifdefempty{\r@license}{}{%
    \recipelicense[\ifdefempty{\r@licenseurl}{}{\r@licenseurl}]{\r@license}}
}

\newcommand{\parsephotocitation}[1]{%
  \gdef\p@creditauthor{}%
  \gdef\p@authorurl{}%
  \gdef\p@license{}%
  \gdef\p@licenseurl{}%
  \gdef\@photocredittext{}%
  \gdef\@photolicensetext{}%
  \pgfkeys{/photo, default, #1}%
  \ifdefempty{\p@creditauthor}{}{%
    \global\togglefalse{alloriginal}%
    \photocredit[\ifdefempty{\p@authorurl}{}{\p@authorurl}]{\p@creditauthor}}%
    \ifdefempty{\p@license}{}{%
    \photolicense[\ifdefempty{\p@licenseurl}{}{\p@licenseurl}]{\p@license}}%
}

\newcommand{\frontcoverphoto}[2][]{%
   \parsephotocitation{#1}
   \def\@frontcoverphoto{#2}
}

\newcommand{\inlinecitationalignment}{\raggedright}
\newcommand{\checkinlinecitation}{%
\iftoggle{inlinecitations}{%
  \ifdefempty{\@recipecredittext}{}{%
    \vspace{-\aftersecskip}
    {\inlinecitationalignment\headingfamily\relsize{-1}\itshape By \@recipecredittext\ifdefempty{\@recipelicensetext}{}{\@recipelicensetext}.\par}
    \vspace{\aftersecskip}}
}{}
}

\iftoggle{endcitations}{\makepagenote}{}
\def\blfootnote{\xdef\@thefnmark{}\@footnotetext}
\renewcommand{\notesname}{Recipe and Photo Sources}
\newcommand{\xpagenote}[1]{%
  \unskip\begingroup\protected@edef\x{\endgroup\noexpand\pagenote{#1}}%
  \scantokens\expandafter{\x\relax}}
\renewcommand*{\notenumintext}[1]{}
\renewcommand*{\notenuminnotes}[1]{}
\renewcommand{\prenoteinnotes}{\item }
\renewcommand{\postnoteinnotes}{}

% For Inserts.
% \DeclareInserts{25}{ad1.jpg, ad2.jpg, ad3.jpg, ad4.jpg}
% \InsertAd will cycle through ad1.jpg -> ad4.jpg everytime
% it is called.
% It'll be called after every 25 recipes.
\newarray\Inserts
\newcounter{advertTotal}
\newcounter{advertIdx}
\newcounter{recipeIdx}
\newcommand{\advertTotal}{\theadvertTotal}
\newcommand{\DeclareInserts}[2]{%
  \def\recipeAdCount{#1}
  \renewcommand*{\do}[1]{%
    \stepcounter{advertTotal}%
    \Inserts(\arabic{advertTotal})={##1}
  }
  \docsvlist{#2}
}
\newcommand{\InsertAd}{%
  \ifnumequal{\theadvertTotal}{0}{}{%
    \thispagestyle{empty}
    \stepcounter{advertIdx}%
    \pgfmathparse{mod(\theadvertIdx - 1,\advertTotal) + 1}
    \pgfmathtruncatemacro{\adIdx}{\pgfmathresult}%
    \checkInserts(\adIdx)
    \ThisTileWallPaper{\paperwidth}{\paperheight}{\cachedata}
    \mbox{}\clearpage
  }
}


%%% Hook for cover page etc
\newcommand{\makecover}[1][]{%
\begin{titlingpage}
\ifdefempty{\@frontcoverphoto}{%
   \maketitle
}{%
   \ThisTileWallPaper{\paperwidth}{\paperheight}{\@frontcoverphoto}
   \mbox{}\clearpage
}
\thispagestyle{empty}
\ifstrequal{#1}{copyrightpage}{\clearpage\makecrpage}{}
\end{titlingpage}%
}

\newcommand{\makecrpage}{%
%\begin{titlingpage}
\mbox{}
\vfill
\relsize{-1}
% TODO Do not produce author line if no author given!
\ifdefempty{\theauthor}{}{%
\translate{preCRAuthor}\theauthor\translate{postCRAuthor}%
}
\ifdefempty{\@frontcoverphoto}{}{%
  \ifdefempty{\@photocredittext}{}{%
    \\
    \translate{preCRCoverPhoto}\@photocredittext\ifdefempty{\@photolicensetext}{}{\@photolicensetext}\translate{postCRCoverPhoto}
  }\\
}
\translate{designsBy}
\\
\\
\translate{generatedWith}\\
\translate{preCRBookLicense}\href{http://creativecommons.org/licenses/by-sa/3.0/}{\translate{CC-BY-SA}}\translate{postCRBookLicense}\\
%\end{titlingpage}%
}

\patchcommand{\mainmatter}{}{%
%%% turn off sectional numberings
\setcounter{secnumdepth}{-1}
\setcounter{tocdepth}{1}
%% set end notes to be an itemised list
\addtonotes{\protect\begin{itemize}[label=\itemmarker,nosep,leftmargin=*]}
}

%%% Overview as a bit of a chapter introduction
\newenvironment{overview}[1]{%
\chapter*{#1}
\providelength{\oldparskip}
\setlength{\oldparskip}{\parskip}
\setlength{\parskip}{\onelineskip}
}{%
\setlength{\parskip}{\oldparskip}
\clearpage
}

\newcommand{\dedication}[1]{%
\cleartoverso\thispagestyle{empty}
\vspace*{\stretch{1}}
\begin{center}
\itshape\Large
#1
\end{center}
\vspace*{\stretch{2}}
}

%%%% TODO cooking time, preparation time, portion... "atomic" fields
\newcommand{\ovenstyle}{}
\newcommand{\oven}[1]{\makebox{\ovenstyle \translate{prePreheatTemperature}#1\translate{postPreheatTemperature}}\par}

\newcommand{\maintextcolor}{black}
\newcommand{\primarycolor}{blue}
\newcommand{\secondarycolor}{blue!80}
\newcommand{\tertiarycolor}{blue!60}
\newcommand{\primarycolorcomplement}{white}
\newcommand{\secondarycolorcomplement}{white}
\newcommand{\tertiarycolorcomplement}{white}
\newcommand{\darkhighlight}{red!80!black}
\newcommand{\brighthighlight}{red}
\newcommand{\highlightcolor}{\darkhighlight}
\newcommand{\externallinkcolor}{SteelBlue}

\newcommand{\scriptdefault}{pzc}
\newcommand{\scriptfamily}{\fontfamily{\scriptdefault}\selectfont}

\newcommand{\recipecolor}{\highlightcolor}
\newcommand{\recipefamily}{\headingfamily}
\newcommand{\recipesize}{\LARGE}
\newcommand{\recipestyle}{\bfseries}
\newcommand{\recipefont}{\recipefamily\recipesize\color{\recipecolor}\recipestyle}

\newcommand{\headingcolor}{\primarycolor}
\newcommand{\headingfamily}{\sffamily}
\newcommand{\headingsize}{\large}
\newcommand{\headingstyle}{\itshape}
\newcommand{\headingfont}{\headingfamily\headingsize\color{\headingcolor}\headingstyle}

\renewcommand{\chaptitlefont}{\recipefamily\color{\recipecolor}\bfseries\HUGE\centering}
\setsecheadstyle{\recipefont}
\setsubsecheadstyle{\headingfont}

\newcommand{\headingmarker}{\textbullet}
\newcommand{\itemmarker}{\textbullet}
\newcommand{\enumeratemarker}[1]{#1.}
\newcommand{\inlinelistmarker}{\makebox[1.5em][c]{\textbullet}}
\newcommand{\inlinelistpunct}{;\space}

\newcommand{\equipmentstyle}{leftmargin=*}
\newcommand{\ingredientsstyle}{label=\textbullet,leftmargin=*}
\newcommand{\preparationstyle}{label=\textbullet,leftmargin=*}
\newcommand{\substitutionstyle}{label=\textbullet,leftmargin=*}
\newcommand{\instructionsstyle}{label={\arabic*. },leftmargin=*}


%%%% Basic structures, redefine as needed in theme files using titlesec and enumitem.
\newcommand{\recipecategory}[1]{%
\clearforchapter
\formatcategorypage{#1}
\phantomsection\addcontentsline{toc}{chapter}{#1}
\clearpage
}

\newcommand{\formatcategorypage}[1]{%
\thispagestyle{empty}
\vspace*{\stretch{1}}
\begin{center}
\fontsize{48pt}{50pt}\headingfamily\bfseries\selectfont\color{\recipecolor}#1
\end{center}
\vspace*{\stretch{2}}
}

\newenvironment{recipe}[2][]{%
   \clearpage%
   \parserecipecitation{#1}
   \color{\maintextcolor}%
   \section{#2}
   \checkinlinecitation
}{}

\renewcommand{\memsecinfo}[5]{%
  \ifstrequal{#1}{section}{\renewcommand{\recipetitle}{#3}}{}
}

\AtBeginEnvironment{recipe}{%
  \def\@recipecredittext{}
  \def\@recipelicensetext{}
  \def\@photocredittext{}
  \def\@photolicensetext{}
  \def\citationtext{}
}

\AtEndEnvironment{recipe}{%
  \ifdefempty{\@recipecredittext}{}{\apptocmd{\citationtext}{\translate{preRecipeCourtesy}\@recipecredittext\ifdefempty{\@recipelicensetext}{}{\@recipelicensetext}\translate{postRecipeCourtesy}}{}{}}
  \ifdefempty{\@photocredittext}{}{\apptocmd{\citationtext}{ \translate{prePhotoCourtesy}\@photocredittext\ifdefempty{\@photolicensetext}{}{\@photolicensetext}\translate{postPhotoCourtesy}}{}{}}
  \iftoggle{footercitations}{%
    \ifdefempty{\citationtext}{}{\blfootnote{\citationtext}}
  }{}
  \iftoggle{endcitations}{%
    \ifdefempty{\citationtext}{}{\leavevmode\xpagenote{\textcolor{\darkhighlight}{\headingfamily\bfseries\recipetitle,}\space\noexpand\hyperlink{page.\thepage}{\thepage}.\enspace\citationtext}}
  }{}
}

\AfterEndEnvironment{recipe}{%
  % Check if an ad should be inserted
  \ifdef{\recipeAdCount}{
    \ifnumgreater{\theadvertTotal}{0}{
      \stepcounter{recipeIdx}
      \pgfmathparse{mod(\therecipeIdx,\recipeAdCount)}
      \pgfmathtruncatemacro{\recIdx}{\pgfmathresult}
      \ifnumequal{\recIdx}{0}{\afterpage{\InsertAd}}{}
    }{}
  }{}
}

\newcommand{\insetphoto}[2][width=.4\textwidth]{%
  \parsephotocitation{#1}
  \includegraphics[width=\@photowidth]{#2}\par
}

\newcommand{\fullpagephoto}[2][]{%
  \iftoggle{inlinecitations}{}{\parsephotocitation{#1}}
  \afterpage{\iftoggle{inlinecitations}{\parsephotocitation{#1}}{}%
    \ThisTileWallPaper{\paperwidth}{\paperheight}{#2}
    \iftoggle{clearpn}{\thispagestyle{empty}}{}
    \iftoggle{inlinecitations}{%
      \ifdefempty{\@photocredittext}{}{%
        \begin{tikzpicture}[overlay,remember picture]
        \node[fill=white,fill opacity=.8,text opacity=1,font=\relsize{-1}\headingfamily\itshape, 
        anchor=north east,xshift=-.5in,yshift=-.5in] 
        at (current page.north east)
        {\translate{prePhotoCourtesy}\@photocredittext\ifdefempty{\@photolicensetext}{}{\@photolicensetext}\translate{postPhotoCourtesy}};
        \end{tikzpicture}
     }
    }{}
    \mbox{}\clearpage}
}

\newcommand{\checkfiller}{%
  \iftoggle{print}{%
    \clearpage
    \checkoddpage\strictpagecheck
%    \ifoddpage\ThisTileWallPaper{\paperwidth}{\paperheight}{photo/filler1}\mbox{}\clearpage\fi
    \ifoddpage\InsertAd\fi
  }{}
}


\newcommand{\SpreadWallPaperLeft}[1]{%
  \sbox0{\includegraphics{#1}}%
  \AddToShipoutPicture*{\put(\LenToUnit{\wpXoffset},\LenToUnit{\wpYoffset}){%
     \parbox[b][\paperheight]{\paperwidth}{%       
       \vfill
       \centering
       \includegraphics[width=\paperwidth,height=\paperheight,clip,trim=0 0 {.5\wd0} 0]{#1}%
       \vfill
     }}}
%  \iftoggle{clearpn}{\thispagestyle{empty}}{}
}  

\newcommand{\SpreadWallPaperRight}[1]{%
  \sbox0{\includegraphics{#1}}%
  \AddToShipoutPicture*{\put(\LenToUnit{\wpXoffset},\LenToUnit{\wpYoffset}){%
     \parbox[b][\paperheight]{\paperwidth}{%       
       \vfill
       \centering
       \includegraphics[width=\paperwidth,height=\paperheight,clip,trim={.5\wd0} 0 0 0]{#1}%
       \vfill
     }}}
  \iftoggle{clearpn}{\thispagestyle{empty}}{}
  \iftoggle{inlinecitations}{%
    \ifdefempty{\@photocredittext}{no}{%
      \begin{tikzpicture}[overlay,remember picture]
        \node[fill=white,fill opacity=.8,text opacity=1,font=\relsize{-1}\headingfamily\itshape, 
        anchor=north east,xshift=-.5in,yshift=-.5in] 
        at (current page.north east)
        {\translate{prePhotoCourtesy}\@photocredittext\ifdefempty{\@photolicensetext}{}{\@photolicensetext}\translate{postPhotoCourtesy}};
      \end{tikzpicture}
    }
  }{}

}  

\newcommand{\landscapedphoto}[2][]{%
  \parsephotocitation{#1}
  \AddToShipoutPicture*{\put(\LenToUnit{\wpXoffset},\LenToUnit{\wpYoffset}){%
     \parbox[b][\paperheight]{\paperwidth}{%       
       \vfill
       \centering
       \includegraphics[width=\paperheight,height=\paperwidth,angle=90]{#2}%
       \vfill
     }}}
  \iftoggle{inlinecitations}{%
    \ifdefempty{\@photocredittext}{}{%
      \begin{tikzpicture}%[overlay,remember picture]
        \node[fill=white,fill opacity=.8,text opacity=1,font=\relsize{-1}\headingfamily\itshape, 
        %anchor=north west,%xshift=.5in,yshift=-.5in
        ] 
        %at (current page.north)
        {Photo courtesy \@photocredittext\ifdefempty{\@photolicensetext}{}{\@photolicensetext}.};
      \end{tikzpicture}
    }
  }{}
}

\newcommand{\preparespread}{%
\iftoggle{print}{}{%
  \setlength{\paperwidth}{2\paperwidth}
  \eject \pdfpagewidth=\paperwidth
}
}
\newcommand{\cancelspread}{%
\iftoggle{print}{}{%
  \setlength{\paperwidth}{0.5\paperwidth}
  \eject \pdfpagewidth=\paperwidth
  }
}

\newcommand{\twopagespreadphoto}[2][]{%
  \iftoggle{inlinecitations}{}{\parsephotocitation{#1}}%
  \iftoggle{print}{%
    \SpreadWallPaperLeft{#2}
    \afterpage{\iftoggle{inlinecitations}{\parsephotocitation{#1}}{}
    \SpreadWallPaperRight{#2}
    \mbox{}\clearpage}
  }{%
    \addtolength{\wpXoffset}{.5\paperwidth}
    \ThisTileWallPaper{\paperwidth}{\paperheight}{#2}%
    \iftoggle{inlinecitations}{%
      \ifdefempty{\@photocredittext}{no}{%
        \begin{tikzpicture}[overlay,remember picture]
          \node[fill=white,fill opacity=.8,text opacity=1,font=\relsize{-1}\headingfamily\itshape, 
          anchor=north east,xshift=-.5in,yshift=-.5in] 
          at (current page.north east)
          {\translate{prePhotoCourtesy}\@photocredittext\ifdefempty{\@photolicensetext}{}{\@photolicensetext}\translate{postPhotoCourtesy}};
        \end{tikzpicture}
      }
    }{}
  }
%%%%%%%%%%% landscaped version %%
%{\afterpage{\iftoggle{inlinecitations}{\parsephotocitation{#1}}{}
%    \begin{landscape}
%    \thispagestyle{empty}
%    \landscapedphoto[#1]{#2}\mbox{}\clearpage
%    \end{landscape}
%  }}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
}

\newcommand{\photo}[2][]{%
\parsephotocitation{#1}
\ifdefstring{\p@embed}{inset}{\insetphoto[#1]{#2}}{}
\ifdefstring{\p@embed}{fullpage}{\fullpagephoto[#1]{#2}}{}
\ifdefstring{\p@embed}{twopagespread}{\twopagespreadphoto[#1]{#2}}{}
}

\AtBeginDocument{%
  \iftoggle{inlinecitations}{%
    % See http://tex.stackexchange.com/a/68741/226
    \expandafter\apptocmd\csname\string\insetphoto\endcsname{%
      \ifdefempty{\@photocredittext}{}{\vskip-\afterinsetphotoskip
        {\centering\relsize{-1}\headingfamily\itshape\translate{prePhotoCourtesy}\@photocredittext\ifdefempty{\@photolicensetext}{}{\@photolicensetext}\translate{postPhotoCourtesy}\par}
        \vskip\afterinsetphotoskip
      }%
    }{}{}
  }{}
}

\newenvironment{equipment}[1][\translate{Equipment}]{%
   \needspace{3\baselineskip}
   \subsection{#1}
   \expandafter\setlist\expandafter{\equipmentstyle}%
   \begin{equipmentlist}
}{%
   \end{equipmentlist}
}

\newenvironment{equipmentlist}{%
   \begin{description}
}{%
   \end{description}
}

\newcommand{\compactequipment}{%
   \renewenvironment{equipmentlist}{%
      \begin{description*}[mode=unboxed,leftmargin=*,itemjoin=\inlinelistpunct]
   }{%
      \end{description*}
   }
}

\newcommand{\normalequipment}{%
   \renewenvironment{equipmentlist}{%
      \begin{description}
   }{%
      \end{description}
   }
}

\newenvironment{preparation}[1][\translate{Preparation}]{%
   \needspace{3\baselineskip}
   \subsection{#1}
   \expandafter\setlist\expandafter{\preparationstyle}%
   \begin{preparationlist}
}{%
   \end{preparationlist}
}

\newenvironment{preparationlist}{%
   \begin{itemize}
}{%
   \end{itemize}
}

\newcommand{\normalpreparation}{%
   \renewenvironment{preparationlist}{%
      \begin{itemize}
   }{%
      \end{itemize}
   }
}

\newcommand{\compactpreparation}{%
   \renewenvironment{preparationlist}{%
      \begin{itemize*}[label={},afterlabel={},itemjoin=\inlinelistpunct]
   }{%
      \end{itemize*}
   }
}


\newenvironment{ingredients}[1][\translate{Ingredients}]{%
   \needspace{3\baselineskip}
   \subsection{#1}%
   \expandafter\setlist\expandafter{\ingredientsstyle}%
   \begin{ingredientlist}
}{%
   \end{ingredientlist}
}

\newcommand{\ingred}[3]{%
\item {#1}\space\notblank{#2}{#2\space}{}{#3}
}

\newenvironment{ingredientlist}{%
   \renewcommand{\ingred}[3]{\item {##1}\space\notblank{##2}{##2\space}{}{##3}}
   \begin{itemize}
}{%
   \end{itemize}
}

\newcommand{\tabularingredients}{%
  \renewcommand{\ingred}[3]{{##1}\space\notblank{##2}{##2}{} & {##3}\\}
  \renewenvironment{ingredientlist}{%
      \begin{tabular}{r l}
  }{%
      \end{tabular}
  }
}

\newcommand{\compactingredients}{%
   \renewcommand{\ingred}[3]{\item {##1}\space\notblank{##2}{##2\space}{}{##3}}
   \renewenvironment{ingredientlist}{%
      \begin{itemize*}[label={},afterlabel={},itemjoin={\inlinelistpunct},noitemsep]
   }{%
      \end{itemize*}
   }
}

\newcommand{\normalingredients}{%
   \renewcommand{\ingred}[3]{\item {##1}\space\notblank{##2}{##2\space}{}{##3}}
   \renewenvironment{ingredientlist}{%
      \begin{itemize}
   }{%
      \end{itemize}
   }
}

\newenvironment{substitution}[1][\translate{Substitutes}]{%
   \needspace{3\baselineskip}
   \subsection{#1}%
   \expandafter\setlist\expandafter{\substitutionstyle}%
   \begin{substitutionlist}
}{%
   \end{substitutionlist}
}

\newenvironment{substitutionlist}{%
   \begin{itemize}
}{%
   \end{itemize}
}

\newcommand{\normalinsubstitutions}{%
   \renewenvironment{substitutionlist}{%
      \begin{itemize}
   }{%
      \end{itemize}
   }
}

\newcommand{\compactinsubstitutions}{%
   \renewenvironment{substitutionlist}{%
      \begin{itemize*}[label={},afterlabel={},itemjoin={;\space},noitemsep]
   }{%
      \end{itemize*}
   }
}


\newenvironment{instructions}[1][\translate{CookingInstructions}]{%
   \needspace{3\baselineskip}
   \subsection{#1}
   \expandafter\setlist\expandafter{\instructionsstyle}%
   \begin{instructionlist}
}{
   \end{instructionlist}
}

\newenvironment{instructionlist}{%
   \begin{enumerate}
}{%
   \end{enumerate}
}

\newcommand{\normalinstructions}{%
   \renewenvironment{instructionlist}{%
      \begin{enumerate}
   }{%
      \end{enumerate}
   }
}

\newcommand{\compactinstructions}{%
   \renewenvironment{instructionlist}{%
      \begin{enumerate*}[label={},afterlabel={},itemjoin=\inlinelistmarker]
   }{%
      \end{enumerate*}
   }
}


\newcommand{\startinstructions}{}

\setlength{\footmarkwidth}{0pt}
\setlength{\footmarksep}{0pt}
\setlength{\footparindent}{0pt}
\renewcommand{\foottextfont}{\headingfamily\itshape\color{\darkhighlight}}
\renewcommand*{\footnoterule}{}

\apptocmd{\backmatter}{%
  \addtonotes{\protect\end{itemize}}
  \iftoggle{endcitations}{\nottoggle{alloriginal}{\printpagenotes}{}}{}
}{}{}

%%% Some useful commands

\newcommand{\framedNum}[1]{\protect\tikz[baseline=-3.5pt]\protect\node[draw=none,fill=\secondarycolor,text=\secondarycolorcomplement,rectangle,aspect=1,minimum width=1.3em,inner xsep=1pt,font=\scriptsize]{#1};}

\newcommand{\circledNum}[1]{\protect\tikz[baseline=-3.5pt]\protect\node[draw=none,fill=\secondarycolor,text=\secondarycolorcomplement,circle,minimum width=1.25em,inner sep=1pt,font=\scriptsize]{#1};}

\newcommand{\triangledNum}[1]{\protect\tikz[baseline=-3.5pt]\protect\node[isosceles triangle,isosceles triangle apex angle=75,shape border rotate=90,draw=none,minimum width=1.5em,inner xsep=1pt,fill=\secondarycolor,font=\scriptsize]{#1};}

\newcommand{\framedPage}{\tikz[overlay]\node[fill=\tertiarycolor,text=\tertiarycolorcomplement,text depth=.2in,minimum height=.5in,minimum width=2em,font=\headingfamily]{\thepage};}


%% Indices nested up to 5 levels
\xindyindex
%\long\def\lettergroup#1\item{\item\textcolor{\darkhighlight}}
%\newcommand{\lettergroup}[1]{\item{\item\textbf{#1}}}
%\let\lettergroupDefault\lettergroup
\renewcommand\subitem{\@idxitem \hspace*{10\p@}}
\renewcommand\subsubitem{\@idxitem \hspace*{20\p@}}
\newcommand\sssubitem{\@idxitem \hspace*{30\p@}}
\newcommand\sssssubitem{\@idxitem \hspace*{40\p@}}
\newcommand\sssssssubitem{\@idxitem \hspace*{50\p@}}

\newcommand{\reduceindexindent}{%
\renewcommand\subitem{\@idxitem }
\renewcommand\subsubitem{\@idxitem \hspace*{10\p@}}
\renewcommand\sssubitem{\@idxitem \hspace*{20\p@}}
\renewcommand\sssssubitem{\@idxitem \hspace*{30\p@}}
\renewcommand\sssssssubitem{\@idxitem \hspace*{40\p@}}
}
