<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
 | Converts a fractional number into a UTF-8 character.
 |
 | @param quantity - The numeric value to convert.
 +-->
<xsl:template name="utf-fraction">
  <xsl:param name="quantity" />

  <xsl:variable name="frac">
    <xsl:value-of select="$quantity mod 1" />
  </xsl:variable>
  <xsl:variable name="whole">
    <xsl:if test="$quantity - $frac != 0">
      <xsl:value-of select="$quantity - $frac" />
    </xsl:if>
  </xsl:variable>

  <xsl:choose>
    <xsl:when test="$frac = '0.5'"><xsl:value-of select="$whole" />\nicefrac{1}{2}</xsl:when>
    <xsl:when test="$frac = '0.333'"><xsl:value-of select="$whole" />\nicefrac{1}{3}</xsl:when>
    <xsl:when test="$frac = '0.666'"><xsl:value-of select="$whole" />\nicefrac{2}{3}</xsl:when>
    <xsl:when test="$frac = '0.25'"><xsl:value-of select="$whole" />\nicefrac{1}{4}</xsl:when>
    <xsl:when test="$frac = '0.75'"><xsl:value-of select="$whole" />\nicefrac{3}{4}</xsl:when>
    <xsl:when test="$frac = '0.2'"><xsl:value-of select="$whole" />\nicefrac{1}{5}</xsl:when>
    <xsl:when test="$frac = '0.4'"><xsl:value-of select="$whole" />\nicefrac{2}{5}</xsl:when>
    <xsl:when test="$frac = '0.6'"><xsl:value-of select="$whole" />\nicefrac{3}{5}</xsl:when>
    <xsl:when test="$frac = '0.8'"><xsl:value-of select="$whole" />\nicefrac{4}{5}</xsl:when>
    <xsl:when test="$frac = '0.166'"><xsl:value-of select="$whole" />\nicefrac{1}{6}</xsl:when>
    <xsl:when test="$frac = '0.833'"><xsl:value-of select="$whole" />\nicefrac{5}{6}</xsl:when>
    <xsl:when test="$frac = '0.125'"><xsl:value-of select="$whole" />\nicefrac{1}{8}</xsl:when>
    <xsl:when test="$frac = '0.375'"><xsl:value-of select="$whole" />\nicefrac{3}{8}</xsl:when>
    <xsl:when test="$frac = '0.625'"><xsl:value-of select="$whole" />\nicefrac{5}{8}</xsl:when>
    <xsl:when test="$frac = '0.875'"><xsl:value-of select="$whole" />\nicefrac{7}{8}</xsl:when>
    <xsl:otherwise><xsl:value-of select="$quantity" /></xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>

